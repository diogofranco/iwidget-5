package eu.iwidget.analytics.utility.sap.app;

/**
 * characterization of different chart types
 * @author kristina_kirsten
 *
 */
public enum ChartType{
	BARCHART, LINECHART, STACKEDCOLUMNCHART, PIECHART, CANDLESTICKCHART
}
