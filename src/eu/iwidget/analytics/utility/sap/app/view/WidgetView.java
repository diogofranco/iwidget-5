package eu.iwidget.analytics.utility.sap.app.view;

import eu.iwidget.analytics.utility.sap.app.networkSimulation.NetworkValueList;



public interface WidgetView {
	
	/**
	 * interface for the widget view
	 * @author kristina_kirsten
	 *
	 */
	interface TabViewListener {
	        void buttonClick(String[] selectedFilter, int TabID);
	}
	    
	public void addListener(TabViewListener listener);
}