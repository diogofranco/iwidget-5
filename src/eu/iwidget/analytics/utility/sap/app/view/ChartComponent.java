package eu.iwidget.analytics.utility.sap.app.view;

import java.util.Observable;
import java.util.Observer;
import com.vaadin.ui.Component;

import eu.iwidget.analytics.utility.sap.app.components.chart.Chart;
import eu.iwidget.analytics.utility.sap.app.model.ChartData;


/**
 * @author I303544
 *
 */
public class ChartComponent implements Observer{

	private ChartData data;
	private Chart chart;
	
	/**
	 * constructor 
	 * @param data the data to be set for the chart
	 */
	public ChartComponent(ChartData data) {
		this.data = data;
		this.data.addObserver(this);
		init();
	}
	
	/**
	 * initializes settings
	 */
	private void init() {
		chart=new Chart(this.data.domId);
		setupChart();
	}

	/**
	 * creates a bar chart out of the given chart data
	 * @return a bar chart component
	 */
	public Component getBarChart(){
		if (data.title.equals("Meter Overview")){
			chart.setChartType("Columns");
		}
		else{
			chart.setChartType("ClusteredColumns");
		}
		return chart;
	}

	public Component getStackedColumnChart() {
		chart.setChartType("StackedColumns");
		return chart;
	}

	/**
	 * creates a line chart out of the given chart data
	 * @return a line chart component
	 */
	public Component getLineChart(){
		chart.setChartType("Lines");
		return chart;
	}
	
	public Component getCandlestickChart(){
		chart.setChartType("Candlesticks");
		return chart;
	}
	
	/**
	 * is triggered if chart data has been changed
	 */
	public void update(Observable chartData, Object chartDataValues) {
		this.data = (ChartData) chartData;
		setupChart();
	}
	
	private void setupChart() {
		chart.setDomId(this.data.domId);
		chart.setLdomId(this.data.ldomId);
		chart.setTitle(this.data.title);
		chart.setNames(this.data.seriesNames);
		chart.setCat(this.data.categories.toArray(new String[0]));
		chart.setVal(this.data.values);
		chart.setxAxisTitle(this.data.xAxisTitle);
		chart.setyAxisTitle(this.data.yAxisTitle);
		chart.setUnitsFromServer(this.data.unitsFromServer);
		chart.setHeight("400");
	}
	
}
