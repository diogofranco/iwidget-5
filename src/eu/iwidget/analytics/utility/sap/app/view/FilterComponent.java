package eu.iwidget.analytics.utility.sap.app.view;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Map.Entry;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gwt.dom.client.Style.Unit;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.shared.ui.slider.SliderOrientation;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Slider;
import com.vaadin.ui.Slider.ValueOutOfBoundsException;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import eu.iwidget.analytics.utility.sap.app.model.FilterData;
import eu.iwidget.analytics.utility.sap.app.model.UploadPatternReceiver;
import eu.iwidget.analytics.utility.sap.app.model.UploadReceiver;

import com.vaadin.ui.Button;


public abstract class FilterComponent{

	/**
	 * creates a new drop down filter
	 * @param data the data to be displayed in the filter
	 * @return the dropdown filter component
	 */
	public static Component getDropdownFilterComponent(FilterData data){
		ComboBox filterElement = new ComboBox(data.title);
		filterElement.setSizeFull();
		Iterator<Entry<Integer, String>> it = data.values.entrySet().iterator();
		while (it.hasNext()) filterElement.addItem(it.next().getValue());
		if(data.id != null) filterElement.setDebugId(data.id);
		
		filterElement.setValue(data.defaultValue);
		return filterElement;
	}	
	
	/**
	 * creates a new checkbox filter
	 * @param data the data to be displayed in the filter
	 * @return the checkbox filter component
	 */
	public static Component getCheckboxFilterComponent(FilterData data){
		OptionGroup filterElement = new OptionGroup(data.title);
		filterElement.setMultiSelect(false);
		Iterator<Entry<Integer, String>> it = data.values.entrySet().iterator();
		while (it.hasNext()) filterElement.addItem(it.next().getValue());
		filterElement.select(data.defaultValue);
		return filterElement;
	}
	
	public static Component getDemandCheckboxFilterComponent(FilterData data){		
		CheckBox filterElement = new CheckBox(data.title);
		filterElement.setValue(false);
		return filterElement;
	}
	
	public static Component getSlider(FilterData data){
		final Slider filterElement = new Slider(data.title);
		filterElement.setOrientation(SliderOrientation.HORIZONTAL);
		filterElement.setWidth(150f, com.vaadin.server.Sizeable.Unit.PIXELS);
		try{
			if(data.title.equals("Minimum Pressure (m)")){
				filterElement.setResolution(1);
				filterElement.setMax(60D);
				filterElement.setMin(20D);
				filterElement.setValue(45.5);
			}
			else if(data.title.equals("Maximum Pressure (m)")){
				filterElement.setResolution(1);
				filterElement.setMax(70D);
				filterElement.setMin(30D);
				filterElement.setValue(48.0);
			}
			else{
				filterElement.setResolution(2);
				filterElement.setMax(0.4D);
				filterElement.setMin(0.01D);
				filterElement.setValue(0.06D);
			}
		} catch(ValueOutOfBoundsException e){
			Notification.show(e.getMessage());
		}		
		return filterElement;	
	}
	
//	public static Component getTextFieldFilterComponent(FilterData data){
//		TextField tf = new TextField(data.title);
//        tf.setValue("Insert "+ data.title);
//       // String value = (String) event.getProperty().getValue();
//        
//        return tf;
//}
	
	/* Used for datepickers in tab "Campaigns" */
	public static Component getCampaignsDatePicker(FilterData data){
		VerticalLayout layout = new VerticalLayout();
		PopupDateField df = new PopupDateField(data.title);
		df.setWidth("100%");
		df.setStyleName("datepicker");
		
		Calendar c = Calendar.getInstance();
		c.set(2014, 9, 8, 00, 00, 00);
		df.setValue(c.getTime());
		df.setResolution(Resolution.MINUTE);
		df.setDateFormat("dd-MM-yyyy HH:mm:ss");
		layout.addComponent(df);
		return layout;
	}
	
	public static Component getTextFieldComponent(FilterData data){
		TextField tf = new TextField(data.title);
		tf.setColumns(13);
		tf.setValue(data.defaultValue.toString());
		return tf;
	}
	/**
	 * creates a new date picker filter with two components: from and to
	 * @param data the data contains the latest value to be displayed
	 * @return the data picker component
	 */
	public static Component getDatePickerFilter(FilterData data){
		VerticalLayout layout = new VerticalLayout();
		Label title = new Label(data.title);
		layout.addComponent(title);
		Calendar d = (Calendar) data.defaultValue;
		
		PopupDateField datetimeFrom = new PopupDateField("From:");
		datetimeFrom.setStyleName("datepicker");
		datetimeFrom.setWidth("100%");
		Calendar fromDate = Calendar.getInstance();
		//Calendar.set --> Month is 0-based, e.g. 0 is January
		fromDate.set(d.get(Calendar.YEAR), d.get(Calendar.MONTH)-1, d.get(Calendar.DAY_OF_MONTH)-7,d.get(Calendar.HOUR_OF_DAY),d.get(Calendar.MINUTE),d.get(Calendar.SECOND));
		datetimeFrom.setValue(fromDate.getTime());
		datetimeFrom.setResolution(Resolution.MINUTE);
		datetimeFrom.setDateFormat("dd-MM-yyyy HH:mm:ss");
		layout.addComponent(datetimeFrom);
		
		PopupDateField datetimeTo = new PopupDateField("To:");
		datetimeTo.setStyleName("datepicker");
		datetimeTo.setWidth("100%");
		Calendar toDate = Calendar.getInstance();
		//I take the values until the last minute of the last day 23:59:59
		toDate.set(d.get(Calendar.YEAR), d.get(Calendar.MONTH)-1, d.get(Calendar.DAY_OF_MONTH),23,59,59);
		datetimeTo.setValue(toDate.getTime());
		datetimeTo.setResolution(Resolution.MINUTE);
		datetimeTo.setDateFormat("dd-MM-yyyy HH:mm:ss");
		layout.addComponent(datetimeTo);
		
		return layout;
	}

	public static Component getUploadComponent(FilterData filterData, UploadReceiver receiver) {
		VerticalLayout layout = new VerticalLayout();
		Upload upload = new Upload(filterData.title, receiver);
		upload.setImmediate(true);
		upload.addSucceededListener(receiver);
		
		layout.addComponent(upload);
		layout.addComponent(receiver.uploadLayout);
		
		return layout;
	}
	
	public static Component getUploadPatternComponent(
			FilterData filterData, UploadPatternReceiver patternReceiver) {
		VerticalLayout layout = new VerticalLayout();
		Upload upload = new Upload(filterData.title, patternReceiver);
		upload.setImmediate(true);
		upload.addSucceededListener(patternReceiver);
		
		layout.addComponent(upload);
		layout.addComponent(patternReceiver.uploadLayout);
		
		return layout;
	}

	public static Component getComplaintsDatePicker(FilterData filterData) {
		VerticalLayout layout = new VerticalLayout();
		PopupDateField df = new PopupDateField(filterData.title);
		df.setWidth(157f, com.vaadin.server.Sizeable.Unit.PIXELS);
		df.setStyleName("datepicker");
		
		Calendar c = Calendar.getInstance();
		c.set(2014, 9, 8, 00, 00, 00);
		df.setValue(c.getTime());
		df.setResolution(Resolution.DAY);
		df.setDateFormat("dd-MM-yyyy");
		layout.addComponent(df);
		return layout;
	}
	
}
