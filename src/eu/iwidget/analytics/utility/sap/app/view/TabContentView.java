package eu.iwidget.analytics.utility.sap.app.view;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.addition.epanet.EPATool;
import org.apache.commons.io.FileUtils;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import eu.iwidget.analytics.utility.sap.app.ChartType;
import eu.iwidget.analytics.utility.sap.app.FilterType;
import eu.iwidget.analytics.utility.sap.app.model.ChartData;
import eu.iwidget.analytics.utility.sap.app.model.FilterData;
import eu.iwidget.analytics.utility.sap.app.model.SceneryFactory;
import eu.iwidget.analytics.utility.sap.app.model.TabContentData;
import eu.iwidget.analytics.utility.sap.app.model.TableWarning;
import eu.iwidget.analytics.utility.sap.app.model.UploadPatternReceiver;
import eu.iwidget.analytics.utility.sap.app.model.UploadReceiver;
import eu.iwidget.analytics.utility.sap.app.view.WidgetView.TabViewListener;
import eu.iwidget.analytics.utility.sap.app.model.Services;
import eu.iwidget.analytics.utility.sap.app.networkSimulation.NetworkValuesReader;

/**
 * @author Diogo Franco
 *
 */
@SuppressWarnings({ "deprecation", "serial" })
public class TabContentView extends CustomComponent implements ClickListener {
	//	private static final String DEFAULT_SEC_TAB = "First"; 
	private HorizontalLayout layout;
	private VerticalLayout filterLayout;
	private VerticalLayout chartLayout;
	private VerticalLayout tableLayout;
	private VerticalLayout simulationsLayout;
	private VerticalLayout complaintsLayout;
	private TabContentData curTabData;

	private List<TabViewListener> listeners; 
	private ChartData[] charts;
	private TableWarning[] tableWarnings = {} ;
	@SuppressWarnings("unused")
	private Tab tab;
	private Button Btn_calculate;
	private Services service = new Services();
	private NetworkValuesReader nvr = null;
	private String[] selectedFilter = null;
	private UploadReceiver uploadReceiver = null;
	private UploadPatternReceiver uploadPatternReceiver = null;
	//	private String secTabChoice=DEFAULT_SEC_TAB;
	
	private final String firstTabName = "WU-UC04.3 - Pressure Overview";
	private final String secondTabName = "WU-UC04.3 - Pressure Statistics";
	private final String thirdTabName = "WU-UC04.3 - Pressure Campaigns";
	private final String fourthTabName = "WU-UC05.2 - Complaints";
	private final String fifthTabName = "WU-UC05.3 - Warnings";
	private final String sixthTabName = "WU-UC07.2 - Network Expansions";
	

	private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


	/**
	 * constructor for creating a new tab content view
	 * @param curTabData the data to be set for a tab content view
	 */
	public TabContentView(TabContentData curTabData){ 
		this.curTabData = curTabData;

		layout = new HorizontalLayout();
		layout.setSizeFull();
		layout.setMargin(true);
		layout.setSpacing(true);

		setLayout();
		fillFilterLayout(curTabData.filters);
		if(curTabData.title.equals(firstTabName) || 
				curTabData.title.equals(secondTabName) ||
				curTabData.title.equals(thirdTabName)){
			fillChartLayout(curTabData.charts);
		}
		else if(curTabData.title.equals(fifthTabName)){
			fillTableLayout();
		}
		else if (curTabData.title.equals(sixthTabName)){
			fillSimulationResultsLayout(curTabData.charts);
		}
		setCompositionRoot(layout);

		listeners = new ArrayList<TabViewListener>();
	}

	/**
	 * sets the layout
	 */
	public void setLayout() {
		filterLayout = new VerticalLayout();
		filterLayout.setMargin(true);
		filterLayout.setSpacing(true);

		layout.addComponent(filterLayout);	
		layout.setExpandRatio(filterLayout, 1);

	}

	/**
	 * fills the filter layout
	 * @param filters array with filters to be set
	 */
	private void fillFilterLayout(FilterData[] filters) {
		for (int i=0; i<filters.length; i++){
			Component component= null;
			if (filters[i].type.equals(FilterType.DROPDOWN)) component = 
					FilterComponent.getDropdownFilterComponent(filters[i]);
			else if (filters[i].type.equals(FilterType.CHECKBOX)) component = 
					FilterComponent.getCheckboxFilterComponent(filters[i]);
			else if (filters[i].type.equals(FilterType.INPUT_FIELD)) component = 
					FilterComponent.getTextFieldComponent(filters[i]);
			else if (filters[i].type.equals(FilterType.DATEPICKER)) component = 
					FilterComponent.getDatePickerFilter(filters[i]);
			else if (filters[i].type.equals(FilterType.CAMPAIGNS_DATE_PICKER)) component = 
					FilterComponent.getCampaignsDatePicker(filters[i]);
			else if (filters[i].type.equals(FilterType.COMPLAINTS_DATE_PICKER)) component = 
					FilterComponent.getComplaintsDatePicker(filters[i]);
			else if (filters[i].type.equals(FilterType.UPLOAD)){
				UploadReceiver receiver = new UploadReceiver(); 
				component = FilterComponent.getUploadComponent(filters[i], receiver);
				this.uploadReceiver = receiver;
			}
			else if (filters[i].type.equals(FilterType.UPLOAD_PATTERN)) {
				UploadPatternReceiver patternReceiver = new UploadPatternReceiver();
				component = FilterComponent.getUploadPatternComponent(filters[i], patternReceiver);
				this.uploadPatternReceiver = patternReceiver;
			}
			else if (filters[i].type.equals(FilterType.SLIDER)) {
				component = FilterComponent.getSlider(filters[i]);
				((Slider) component).addValueChangeListener(
						new Property.ValueChangeListener(){
							@Override
							public void valueChange(ValueChangeEvent event) {
								if(nvr != null){
									if(((Slider) event.getProperty()).getCaption().equals("Minimum Pressure (m)")){
										nvr.drawMinPressureChart(curTabData.charts[0], 
												(Double) event.getProperty().getValue(),
												selectedFilter);
									}
									else if(((Slider) event.getProperty()).getCaption().equals("Maximum Pressure (m)")){
										nvr.drawMaxPressureChart(curTabData.charts[1], 
												((Slider) filterLayout.getComponent(5)).getValue(),
												(Double) event.getProperty().getValue(),
												selectedFilter);
									}
									else if(((Slider) event.getProperty()).getCaption().equals("Velocity (m/s)")){
										nvr.drawMinVelocityChart(curTabData.charts[2], 
												(Double) event.getProperty().getValue(),
												selectedFilter);
									}
									fillSimulationResultsLayout(curTabData.charts);
								}	
							}	
						});
			}
			else if (filters[i].type.equals(FilterType.DEMAND_CHECKBOX)) {
				component = FilterComponent.getDemandCheckboxFilterComponent(filters[i]);
				((CheckBox) component).addValueChangeListener(
						new Property.ValueChangeListener(){

							@Override
							public void valueChange(ValueChangeEvent event) {
								Boolean isSelected = (Boolean) event.getProperty().getValue();
								
								if(isSelected){
									filterLayout.getComponent(2).setCaption("Change Demand From");
									filterLayout.getComponent(3).setVisible(true);
									filterLayout.getComponent(4).setVisible(true);
								}
								else{
									filterLayout.getComponent(2).setCaption("Change Demand");
									filterLayout.getComponent(3).setVisible(false);
									filterLayout.getComponent(4).setVisible(false);
								}
								
							}
						}
				);
			}	
			//component.setStyleName("filter");
			filterLayout.addComponent(component);
		}
		
		if(curTabData.title.equals(sixthTabName) && 
				!filterLayout.getComponent(2).getCaption().equals("Change Demand From")){
			filterLayout.getComponent(3).setVisible(false);
			filterLayout.getComponent(4).setVisible(false);
		}
		
		if (filters.length > 0)  {
			Btn_calculate = new Button("calculate");
			Btn_calculate.addClickListener(this);
			filterLayout.addComponent(Btn_calculate);		
		}
	}

	private void fillTableLayout() {
		if (this.tableLayout!=null) this.layout.removeComponent(tableLayout);
		this.tableLayout = new VerticalLayout();
		tableLayout.setMargin(true);
		tableLayout.setSpacing(true);
		Table table = new Table();
		table.addContainerProperty("Device Id", String.class, null);
		table.addContainerProperty("Possibly Leaking For", String.class, null);
		table.addContainerProperty("Minimum Value", String.class, null);
		table.addContainerProperty("Severity", String.class, null);

		//inserts results into the table.
		for(TableWarning tw : this.tableWarnings){
			Object newItemId = table.addItem();
			Item row = table.getItem(newItemId);
			row.getItemProperty("Device Id").setValue(tw.getDeviceId());
			row.getItemProperty("Possibly Leaking For").setValue(viewLeakingFor(tw.getLeakingFor()));
			row.getItemProperty("Minimum Value").setValue(viewDouble(tw.getMinimumValue()));
			row.getItemProperty("Severity").setValue(tw.getSeverity());
		}

		if(table.size() < 11) { // magic number 11: maximum number of results showing
			table.setPageLength(table.size());
		} else{
			table.setPageLength(10);
		}

		tableLayout.addComponent(table);
		this.layout.addComponent(tableLayout);
	}

	private void fillSimulationResultsLayout(ChartData[] charts) {
		if(this.simulationsLayout!=null) this.layout.removeComponent(simulationsLayout);
		this.simulationsLayout = new VerticalLayout();
		this.simulationsLayout.setMargin(true);
		this.simulationsLayout.setSpacing(true);
		
		this.charts = charts;
		this.simulationsLayout.addStyleName("chartContainer");		
		this.layout.addComponent(simulationsLayout);
		this.layout.setExpandRatio(simulationsLayout, 6);
		
		for(int i = 0; i<charts.length;i++){
			VerticalLayout curChart = new VerticalLayout();
			ChartComponent cComponent = new ChartComponent(charts[i]);
			
			Component component = cComponent.getLineChart();
			curChart.addComponent(component);
			curChart.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
			this.simulationsLayout.addComponent(curChart);	
		}	
	}

	/**
	 * fills the chart layout
	 * @param charts array with the charts to be set
	 */
	public void fillChartLayout(ChartData[] charts) {
		this.charts = charts;

		if (this.chartLayout!=null) this.layout.removeComponent(chartLayout);

		this.chartLayout = new VerticalLayout();
		this.chartLayout.addStyleName("chartContainer");
		this.chartLayout.setSpacing(true);

		this.layout.addComponent(chartLayout);
		this.layout.setExpandRatio(chartLayout, 6);

		for (int i=0; i<charts.length; i++){
			VerticalLayout curChart = new VerticalLayout();
			ChartComponent cComponent = new ChartComponent(charts[i]);

			Component component = null;
			if (charts[i].type.equals(ChartType.BARCHART)) component = cComponent.getBarChart();
			else if (charts[i].type.equals(ChartType.LINECHART)) component = cComponent.getLineChart();
			else if (charts[i].type.equals(ChartType.STACKEDCOLUMNCHART)) component = cComponent.getStackedColumnChart();
			else if (charts[i].type.equals(ChartType.CANDLESTICKCHART)) component = cComponent.getCandlestickChart();
			else {System.out.println("No chart type selected"); return;}

			curChart.addComponent(component);			
			curChart.setComponentAlignment(component, Alignment.MIDDLE_CENTER);

			this.chartLayout.addComponent(curChart);
		}

		HorizontalLayout buttonLayout = new HorizontalLayout();
		this.chartLayout.addComponent(buttonLayout);
		this.chartLayout.setExpandRatio(buttonLayout, 1f);
	}


	/**
	 * is triggered if calculate button is clicked
	 * The first time that this method is used is in order for the initial chart to be presented.
	 * Since there was no ButtonClick, there was no ClickEvent, so event==null (and causes NullPOinterException)
	 * After the first time, the next charts are created by event.getButton()==calculate
	 */
	public void buttonClick(ClickEvent event) {
		//if (this.curTabData.title!="Welcome" && this.curTabData.title!="Performance" && this.curTabData.title!="Benchmarking" && this.curTabData.title!="Consumption Analysis" && this.curTabData.title!="Forecasting") {
		if (event==null || event.getButton()==null|| event.getButton()==Btn_calculate){// I should check it.. Cannot do event.getButton() if event==null
			calculateClickEvent(event); }
		//			}else if (event.getButton()==Btn_timeSeries){
		//				secTabSelection("timeSeries");
		//			}else if (event.getButton()==Btn_expBeh){
		//				secTabSelection("expBeh");
		//			}else if (event.getButton()==Btn_optPress){
		//				secTabSelection("optPress");
		//			}else{
		//				System.out.println("The event does not correspond to a button");
		//			}
		//			hideUnncessaryFilters();
	}


	/**
	 * It is a calculate click event. Find the selected dates from filters. If selected filters is null return. 
	 * Remove all the chartLayouts and fill it again with chartLayouts without values (?)
	 * For all the listeners trigger ButtonClick for the given selectedFilters and the specific TabID
	 * @param action 
	 */
	private void calculateClickEvent(ClickEvent event) {
		FilterData[] filters = curTabData.filters;
		int filterAmount = filters.length;
		if (FilterData.contains(filters, FilterType.DATEPICKER)) 
			filterAmount++;
		String[] selectedFilter = null;
		if ((selectedFilter=getSelectedFilter(filters,filterAmount))==null){
			return;
		}
		for(TabViewListener l : listeners){
			if(curTabData.title.equals(firstTabName) || 
					curTabData.title.equals(secondTabName) || 
					curTabData.title.equals(thirdTabName)){
				this.chartLayout.removeAllComponents();
				fillChartLayout(this.charts);
				l.buttonClick(selectedFilter, this.curTabData.id);
				
			}
		}
		if (curTabData.title.equals(sixthTabName)){
				if(this.simulationsLayout!=null) this.layout.removeComponent(simulationsLayout);
				//test if filter input from user makes sense
				boolean filtersAreCorrect = checkFilters(selectedFilter);
				if(!filtersAreCorrect) return;
				

				//this.simulationsLayout.removeAllComponents();
		
				if(uploadReceiver.fileUploaded == null){ //no upload done yet
					if(event != null && event.getButton() != null){ //if event was fired by button-click
						Notification.show("Please upload a file first!");
						return;
					}
					try {
						new File(UploadReceiver.folderName).mkdir(); //make folder "uploads" if it doesn't exist
						FileUtils.cleanDirectory(new File(UploadReceiver.folderName)); //why not clean temp directory here, given no uploads were made yet
					} catch (IOException e) {
						Notification.show("An error occurred while preparing the temporary folders!");
					}
					return;
				}
				//make different sceneries (.inp files)
				SceneryFactory sf = new SceneryFactory();
				sf.makeScenary(uploadReceiver.fileUploaded,
						selectedFilter, uploadPatternReceiver.patternUploaded);
				
				
				this.selectedFilter = selectedFilter;
				
				//create .out files with EPANet tool
				for(String file : SceneryFactory.sceneryFiles){
					String[] filepath = new String[1];
					filepath[0] = file;
					EPATool.main(filepath);
				}
				
				//read files into memory, calculating the weighted-averages
				nvr = new NetworkValuesReader();
				nvr.readAll();
				
				//read reference values and draw the charts
				Double hMin = Double.parseDouble(selectedFilter[5]);
				Double hMax = Double.parseDouble(selectedFilter[6]);
				Double vRef = Double.parseDouble(selectedFilter[7]);
				nvr.drawMinPressureChart(this.curTabData.charts[0], hMin, selectedFilter);
				nvr.drawMaxPressureChart(this.curTabData.charts[1], hMin, hMax, selectedFilter);
				nvr.drawMinVelocityChart(this.curTabData.charts[2], vRef, selectedFilter);
				fillSimulationResultsLayout(this.curTabData.charts);
			}
			else if(curTabData.title.equals(fifthTabName)){
				this.tableLayout.removeAllComponents();
				this.tableWarnings = service.fillTableData(selectedFilter);
				fillTableLayout();
			}
			
			else if(curTabData.title.equals(fourthTabName)){
				if(this.complaintsLayout != null) this.complaintsLayout.removeAllComponents();
				if(selectedFilter[0] == null || selectedFilter[0].isEmpty()) {
					if(event != null && event.getButton() != null){ //event was fired by button click
						Notification.show("Please insert a client id!");
					}
					return;
				}
				
				String result = service.getComplaintsResult(selectedFilter);
				fillComplaintsLayout(result, selectedFilter[0]);
			}
	}

	private void fillComplaintsLayout(String result, String clientName){
		
		if (this.complaintsLayout!=null) this.layout.removeComponent(complaintsLayout);
		this.complaintsLayout = new VerticalLayout();
		this.complaintsLayout.setMargin(true);
		this.complaintsLayout.setSpacing(true);
		
		Label label = new Label(result);
		Panel panel = new Panel();
		panel.setWidth("275px");
		panel.setHeight("80px");
		panel.setContent(label);
		panel.setCaption("Warnings for client \"" + clientName + "\"");
		
		this.complaintsLayout.addComponent(panel);
		this.complaintsLayout.setSizeFull();
		this.complaintsLayout.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
		this.layout.addComponent(this.complaintsLayout);
	}
	
	/**
	 * @param selectedFilter
	 * @return true if filters selected by user are correct, 
	 * 	meaning the "to" field is bigger then the "from" field and the interval
	 * 	has a 0 remainder with the difference between "to" and "from".
	 */
	private boolean checkFilters(String[] selectedFilter) {
		//filters are correct if we aren't trying to analyze a range.
		if(selectedFilter[1].equals("false")) return true;
		
		Integer from = new Integer((selectedFilter[2].replace("%","")).replace("+", ""));
		Integer to = new Integer((selectedFilter[3].replace("%","")).replace("+", ""));
		Integer steps = new Integer((selectedFilter[4].replace("%","")).replace("+", ""));
		
		if(from >= to || ((to - from) % steps) != 0) {
			Notification.show("Error, make sure your input for the demand ranges is correct!");
			return false;
		}

		return true;
	}

	/**
	 * adds listener for this tab
	 * @param listener the listener for one specific event in a tab
	 */
	public void addListener(TabViewListener listener) {
		this.listeners.add(listener);
	}


	/**
	 * @param filters is an array of FilterData
	 * @param filterAmount the number of filters
	 * @return an array of String e.g. ["15 Min","DMA 1", "2009-12-13 02:21:16", "2009-12-19 15:41:56"]
	 */
	private String[] getSelectedFilter(FilterData[] filters, int filterAmount) { 
		String[] selectedFilter= new String[filterAmount];
		for (int i=0; i<filterAmount; i++){
			if (filters[i].type.equals(FilterType.DROPDOWN) || 
					filters[i].type.equals(FilterType.CHECKBOX) || 
					filters[i].type.equals(FilterType.INPUT_FIELD)) {
				selectedFilter[i] = (String) ((AbstractField) filterLayout.getComponent(i)).getValue();
			} 
			else if (filters[i].type.equals(FilterType.SLIDER)){
				selectedFilter[i] = ((AbstractField) filterLayout.getComponent(i)).getValue().toString();
			}
			else if (filters[i].type.equals(FilterType.DATEPICKER)){
				VerticalLayout v = (VerticalLayout) filterLayout.getComponent(i);
				Iterator<Component> c = v.iterator();
				Date dateFrom=null, dateTo=null;
				while (c.hasNext()) {
					Component com = c.next();
					try {
						if (com.getCaption()=="From:") {
							dateFrom = (Date) ((PopupDateField) com).getValue();
							selectedFilter[i]= dateFormatter.format(dateFrom);
						}
						if (com.getCaption()=="To:") {
							i++;
							dateTo = (Date) ((PopupDateField) com).getValue();
							selectedFilter[i]= dateFormatter.format(dateTo);
						}
					} catch (Exception e) {
						System.out.println("Error"+e.getMessage());
					}
				}
				//If the "From Date" is bigger than the "To Date", return null
				if (checkDatesWarning(dateFrom, dateTo)){
					return null;
				}
			}
			else if (filters[i].type.equals(FilterType.CAMPAIGNS_DATE_PICKER)){
				VerticalLayout v = (VerticalLayout) filterLayout.getComponent(i);
				Date date = (Date) ((PopupDateField) v.getComponent(0)).getValue();
				selectedFilter[i] = dateFormatter.format(date);
			}
			else if(filters[i].type.equals(FilterType.UPLOAD) ||
					filters[i].type.equals(FilterType.UPLOAD_PATTERN)){
				selectedFilter[i] = "";
			}
			else if(filters[i].type.equals(FilterType.DEMAND_CHECKBOX)){
				boolean isChecked = ((CheckBox) filterLayout.getComponent(i)).getValue();			
				selectedFilter[i] = isChecked ? "true" : "false";
			}
			else if(filters[i].type.equals(FilterType.COMPLAINTS_DATE_PICKER)){
				VerticalLayout v = (VerticalLayout) filterLayout.getComponent(i);
				Date date = (Date) ((PopupDateField) v.getComponent(0)).getValue();
				SimpleDateFormat complaintsDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
				selectedFilter[i] = complaintsDateFormatter.format(date);
			}
			else{ 
				throw new UnsupportedOperationException("Filter type is not supported");
			}
		}
		return selectedFilter;
	}
	
	/**
	 * @param d A double in scientific notation
	 * @return A string with the double in scientific notation but only 3 decimal places.
	 */
	public String viewDouble(Double d) {
		String s = d.toString();
		String beggining = s.substring(0, 5);
		String end = s.substring(s.length()- 3);
		return beggining + " " + end;
	}
	

	/**
	 * @param i An integer to print in the leakingFor column
	 * @return A string with the number of days/day to put in the table
	 */
	public String viewLeakingFor(Integer i){
		if(i > 1) return i.toString() + " days";
		if(i == 1) return i.toString() + " day";
		return "An error has occured!";
	}

	//Throw warning if the "From Date" is later than the "To Date"
	private boolean checkDatesWarning(Date dateFrom, Date dateTo) {
		if (dateTo.before(dateFrom)){
			com.vaadin.ui.Notification.show("The second date is earlier than the first date.");
			return true;
		}
		return false;
	}

	/**
	 * gets the current tab data
	 * @return the current tab content data
	 */
	public TabContentData getCurTabData(){
		return this.curTabData;
	}

	//setter
	/**
	 * sets a tab
	 * @param curTab the tab to be set
	 */
	public void setTab(Tab curTab) {
		this.tab = curTab;
	}

}



