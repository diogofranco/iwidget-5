package eu.iwidget.analytics.utility.sap.app.view;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

import eu.iwidget.analytics.utility.sap.app.model.ChartData;
import eu.iwidget.analytics.utility.sap.app.model.TabContentData;


public class WidgetViewImpl extends CustomComponent implements WidgetView {

	private static final long serialVersionUID = 1L;
	private VerticalLayout layout=null;
	private TabSheet tabsheet;
	private Map<Tab, TabContentView> tabViews;
	private Label label;
	
	private static int c = 0;
	
	/**
	 * set up the tabsheet 
	 * create instances for tabs
	 */
	public WidgetViewImpl() {
		layout = new VerticalLayout();
		setCompositionRoot(layout);
		
		tabsheet = new TabSheet();
		
		tabsheet.setSizeFull();
		
		layout.addComponent(tabsheet);
		tabViews = new HashMap<Tab,TabContentView>();
		
			// initial loading of tab content
		SelectedTabChangeListener listener = new SelectedTabChangeListener(){
			private static final long serialVersionUID = 1L;

			public void selectedTabChange(SelectedTabChangeEvent event) {
				((TabContentView)tabsheet.getSelectedTab()).buttonClick(null);
			}
		};
		tabsheet.addListener(listener);
	}

	/**
	 * sets up the tabs with its data
	 * @param tabs the data for each tab to be created
	 */
	public void setupLayout(TabContentData[] tabs){
		int amountTabs = tabs.length;
		for (int n=0; n<amountTabs; n++){
			addTabContent(tabs[n].title, tabs[n]);
		}
	}
	
	/**
	 * adds a tab to the tabsheet
	 * @param name the name for the tab
	 * @param tabData the data to be stored for the tab
	 */
	public void addTabContent(String name, TabContentData tabData){
		TabContentView tabContView = new TabContentView(tabData);
		Tab tab = tabsheet.addTab(tabContView, name);
		tabContView.setTab(tab);
		
		tabViews.put(tab, tabContView);
	}


	/**
	 * sets the chart data
	 * important for updating the chart data
	 * @param valueMapsList the values for the chart
	 * @param tab the tab where the chart is located
	 */
	public void setChartData(HashMap<Date, Double>[] valueMapsList, Tab tab){
		TabContentView curTab = tabViews.get(tab);
		TabContentData d = curTab.getCurTabData();
		ChartData[] charts = d.charts;
		
		curTab.fillChartLayout(charts); 
	}
	
	/**
	 * adds a listener list
	 */
	public void addListener(TabViewListener listener) {
		Iterator<Entry<Tab, TabContentView>> it = this.tabViews.entrySet().iterator();
		while (it.hasNext()) it.next().getValue().addListener(listener);
	}

	/**
	 * 
	 * @param text text which should be set on a label
	 */
	public void addText(String text){
		label.setCaption(""+(c++));
		Object oldText = label.getValue();
		label.setValue(oldText+", "+text);
	}
}