package eu.iwidget.analytics.utility.sap.app;

/**
 * identify different filter categories
 * @author kristina_kirsten
 *
 */
public enum FilterCategory {
	TIME_PERIOD, TIME_INTERVAL, CONSUMER_TYPE, CONSUMER_GROUP, NETWORK_SECTOR,
		TIME_SERIES, METER_TYPE_ID, CONSUMPTION_CATEGORY, PRESSURE_VALUE,TOLERANCE, UPLOAD, DEMAND, COMPLAINTS
}
