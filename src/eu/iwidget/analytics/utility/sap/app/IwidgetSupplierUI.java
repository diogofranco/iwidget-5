package eu.iwidget.analytics.utility.sap.app;

import java.io.IOException;
import java.util.Properties;

//import javax.servlet.annotation.WebServlet;


import com.vaadin.annotations.JavaScript;
//import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import eu.iwidget.analytics.utility.sap.app.model.WidgetModel;
//import eu.iwidget.analytics.utility.sap.app.model.WidgetModelImplHANA;
//import eu.iwidget.analytics.utility.sap.app.model.WidgetModelImplMySQL;
import eu.iwidget.analytics.utility.sap.app.model.WidgetModelImplREST;
import eu.iwidget.analytics.utility.sap.app.view.WidgetViewImpl;


//The Push is commented out because it is not compatible with the Portlet (2.4)
//@Push //enables the server to push state changes to the client without a request initiated by the client. 
@SuppressWarnings("serial")
@Theme("reindeer") //the theme we use
@JavaScript({"http://ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojo/dojo.js",})
public class IwidgetSupplierUI extends UI {
	
//	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = IwidgetSupplierUI.class)
	public static class Servlet extends VaadinServlet {
	}

	/**
	 * initialization of application
	 */
	private Properties config = new Properties();
	private String dbtype; //it is used to choose which database type we use

	@Override
	protected void init(VaadinRequest request) {
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeUndefined();
		layout.setVisible(true);
		
		//Javadoc: calling Class.forName(String) returns the Class object associated with the class or interface with the given string name.
		//It obtains a reference to the class object with the fully qualified class name com.sap.db.jdbc.Driver
		//ngdbc.jar is the SAP HANA jdbc driver to connect Java with the database.
		try {
			Class.forName("com.sap.db.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}		
		
		WidgetViewImpl view = new WidgetViewImpl();

		try {
			config.load(this.getClass().getClassLoader().getResourceAsStream("settings.properties"));
			dbtype = config.getProperty("DATABASE_TYPE");
			
			WidgetModel model = null;
			model = (WidgetModelImplREST) new WidgetModelImplREST();

			layout.addComponent(view);
			new WidgetPresenter(model, view);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("IOException"+e.getMessage());
		}
		
	}
}
