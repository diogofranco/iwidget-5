package eu.iwidget.analytics.utility.sap.app.model;

/**
 * This class is a bag to hold a result from the remote query about device leakage (query 56).
 * 
 * @author Diogo Franco
 */
public class DeviceDataPoint {
    
	public Double value;
    public String deviceID;
    public String date;
    public Integer count;
    
    public DeviceDataPoint(Double value, String deviceID, String date, Integer count) {
    	this.value = value;
    	this.deviceID = deviceID;
    	this.date = date;
    	this.count = count;
    }
}
