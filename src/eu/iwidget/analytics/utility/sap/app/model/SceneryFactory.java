package eu.iwidget.analytics.utility.sap.app.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.ui.Notification;

public class SceneryFactory {
	private static String DEMAND_MULTIPLIER_IDENTIFIER = "Demand M";
	public static int NR_SCENERIES;
	public static String[] sceneryFiles;

	public void makeScenary(String sourceFile,
			String[] selectedFilters, String sourcePattern){
		BufferedReader br = null;
		PrintWriter pw = null; 
		sceneryFiles = null;
		Double[] choosenMultiplier;
		ArrayList<Double> patternValues = new ArrayList<Double>();
		
		/* User uploaded pattern if sourcePattern isn't null */
		if(sourcePattern != null){
			/* Getting values from pattern file to memory */
			try {
				br = new BufferedReader(new FileReader(sourcePattern));
				
				String line;

				/* Kinda horrible - Skipping 2 lines */
				br.readLine();
				br.readLine();
				
				while ((line = br.readLine()) != null) {
					patternValues.add(Double.parseDouble(line));
				}
				
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			/* Creating new file which is a copy on .inp but with the new pattern values */
			try{
				br = new BufferedReader(new FileReader(sourceFile));
				pw =  new PrintWriter(new FileWriter(sourceFile + ".pat"));

				String line;
				while ((line = br.readLine()) != null) {
					if(!line.contains("[PATTERNS]")){
						pw.println(line);
						continue;
					}
					break;
				}
				
				if(line == null){
					Notification.show("Sorry, there seems to be a problem with the .inp file!");
					br.close();
					pw.close();
					return;
				}
				
				pw.println(line); //print [patterns] line
				
				while((line = br.readLine()).contains(";")) {
					pw.println(line);
				}
				
				for(int i = 0; i < 16; i++){
					String newLine = 
							" 1               \t" + patternValues.get((i*6)+0) + 
							"        \t" + patternValues.get((i*6)+1) + 
							"        \t" + patternValues.get((i*6)+2) +
							"        \t" + patternValues.get((i*6)+3) +
							"        \t" + patternValues.get((i*6)+4) + 
							"        \t" + patternValues.get((i*6)+5) + 
							"        ";
					pw.println(newLine);
					line = br.readLine();
				}
				
				/* Copy the rest of the file */
				pw.println(line);
				while((line = br.readLine()) != null){
					pw.println(line);
				}
				br.close();
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			sourceFile = sourceFile + ".pat";
		}
		
		if(selectedFilters[1].equals("false")){ //only 1 scenery
			NR_SCENERIES = 1;
			choosenMultiplier = new Double[NR_SCENERIES];
			sceneryFiles = new String[NR_SCENERIES];
			choosenMultiplier[0] = new Double((selectedFilters[2].replace("%", "")).replace("+", ""));
			choosenMultiplier[0] = choosenMultiplier[0] / 100;
			sceneryFiles[0] = UploadReceiver.folderName + File.separator + 
					"scenery0" + ".inp";
		}
		else{ //multiple sceneries
			Integer from = new Integer((selectedFilters[2].replace("%","")).replace("+", ""));
			Integer to = new Integer((selectedFilters[3].replace("%","")).replace("+", ""));
			Integer steps = new Integer((selectedFilters[4].replace("%","")).replace("+", ""));

			NR_SCENERIES = ((to-from) / steps) + 1;
			choosenMultiplier = new Double[NR_SCENERIES];
			for(int i=0;i<NR_SCENERIES;i++){
				choosenMultiplier[i] = ((from.doubleValue() + (i * steps.doubleValue())) / 100); 
			}
			
			sceneryFiles = new String[NR_SCENERIES];
			for(int i=0; i<NR_SCENERIES; i++){
				sceneryFiles[i] = UploadReceiver.folderName + File.separator + 
						"scenery" + i + ".inp";
			}
			
		}

		for(int i=0; i < NR_SCENERIES; i++){
			try {
				br = new BufferedReader(new FileReader(sourceFile));
				pw =  new PrintWriter(new FileWriter(sceneryFiles[i]));

				String line;
				while ((line = br.readLine()) != null) {
					if(line.contains(DEMAND_MULTIPLIER_IDENTIFIER)){
						String oldMultiplierString = StringUtils.substringAfterLast(line, "\t");
						Double oldMultiplier = new Double(oldMultiplierString);

						Double newMultiplier = (Double) (oldMultiplier + 
								(oldMultiplier * choosenMultiplier[i]));
						DecimalFormatSymbols dfs = new DecimalFormatSymbols();
						dfs.setDecimalSeparator('.');
						NumberFormat formatter = new DecimalFormat("########.############",dfs);
						formatter.setMinimumIntegerDigits(0);
						String s = formatter.format(newMultiplier);
						pw.println(" Demand Multiplier  \t" + s);
						continue;
					}

					pw.println(line);
				}
				br.close();
				pw.close();
			}catch (Exception e) {
				e.printStackTrace();
				Notification.show("An error occured when creating a new scenary!");
			}
		}
	}
}
