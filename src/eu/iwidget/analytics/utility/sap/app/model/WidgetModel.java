package eu.iwidget.analytics.utility.sap.app.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;


/**
 * interface of model for database interactions
 * @author kristina_kirsten
 *
 */
public interface WidgetModel {
	
	public int getMeterCount();
	public int getDMACount();
	public int getHouseholdTypeCount();
	public void setupDBconnection();
	public DBData getConsumerTypes();
	public DBData getPercentile (String timeSerSel, String dmaSelected, String period, String startTime, String endTime);
	public DBData[] firstTab (String timeSerSel,String dma,String period, String from, String to);
	public DBData[] secondTab (String timeSerSel,String dma,String period, String from, String to);
	
	// jpcoelho: change thirdTab declaration to have only 2 campaigns
	//	public DBData thirdTab (String timeSerSel,String dma, String period, String from1, String to1, String from2, String to2, String from3, String to3, String from4, String to4, String from5, String to5);
		public DBData thirdTab (String timeSerSel,String dma, String period, String from1, String to1, String from2, String to2, String from3, String to3);
	//public DBData[] getLeakage (String timeSerSel,String dma,String period, String from, String to);
//	public DBData getTypeTimeSeries (String timeSerSel,String dma,String period, String from, String to);
	//public DBData getBarData (String timeSerSel, String dmaSelected, String period, String from, String to);
	//public DBData getLeakage (String timeSerSel,String dma,String period, String from, String to);
	//public DBData[] getOverallConsAll(String time_period);
	//public DBData getOverallConsumption(String time_period);
	public DBData getConsumptionCategories(String dma, String resolution, String from, String to);
	public DBData getConsumptionCategoriesPerDMA(String from, String to);
	//public DBData getInOutFlow(String dma, String resolution, String from, String to);
	//public DBData[] getInOutFlowAll(String dma, String resolution, String from, String to);
	//public DBData[] getConsCatAll(String dma, String resolution, String from, String to);
	//public DBData getInOutFlowPerDMA(String from, String to);
	//public DBData getFlowPerMeter(String from, String to);
	//public DBData getFlow(String meter, String resolution, String from, String to);
	
	/**
	 * container for database data
	 *
	 */
	public class DBData{
		public LinkedHashSet<String> names;
		public LinkedHashSet<String> categories;
		public Double[][] values;
		public ArrayList<String> unitsFromServer;

		
		/**
		 * constructor for database data
		 * @param categoryNumber the amount of different series in chart
		 */
		public DBData(int categoryNumber) {
			this.names = new LinkedHashSet<String>();
			this.categories = new LinkedHashSet<String>();
			this.values = new Double[categoryNumber][];
			this.unitsFromServer = new ArrayList<String>();
		}
		
	}
	
	
}

