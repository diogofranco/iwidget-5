package eu.iwidget.analytics.utility.sap.app.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



/**
 * This class contains an Arraylist devicelist that includes a set of devices.
 * The devices are of the types: Inflow, Outflow, Output, City, Zone
 * @author I303544
 *
 */


public class DeviceList {
	public static final String DeviceTypeInflow = "Inflow";
	public static final String DeviceTypeOutflow = "Outflow";
	public static final String DeviceTypeOutput = "Output";
	public static final String DeviceTypeZone = "Zone";
	public static final String DeviceTypeCity = "City";
	public static final String DeviceTypeBusiness = "Business";
	public static final String DeviceTypeDomestic = "Domestic";
	public static final String DeviceTypePublicOrganisation = "Public organisation";
	public static final String DeviceTypeOther = "Other";
	public static final String DeviceTypeUnknown = "Unknown";
	public static final String DeviceTypeNull = "null";
	private ArrayList<Device> deviceList;
	Services services=new Services();

	
	/**
	 * create empty DeviceList
	 */
	public DeviceList() {
		this.deviceList = new ArrayList<Device>();
	}
	
	/**
	 * @param str 
	 * create DeviceList with all the devices
	 */
	public DeviceList(String str) {
		if (str.equals("all"))
			deviceList = new ArrayList<Device>(this.services.getAllDevices());
	}

	/**
	 * @param devType: Inflow, Outflow, Output, City, Zone
	 * @return DeviceList with a subset of the initial devices
	 */
	public DeviceList getSubDevList(String dmaSelected) {
		DeviceList dl = new DeviceList();
		dl.setDeviceList( getDevListPerDevId(dmaSelected) );
		return dl;
	}

	/**
	 * @param devType1: Inflow, Outflow, Output, City, Zone
	 * @param devType2: Inflow, Outflow, Output, City, Zone
	 * @return DeviceList with a subset of the initial devices
	 */
//	public DeviceList getSubDevList(String devType1, String devType2) {
//		DeviceList dl = new DeviceList();
//		ArrayList<Device> tmp = new ArrayList<Device>(getDevListPerDevType(devType1));
//		tmp.addAll(getDevListPerDevType(devType2));
//		dl.setDeviceList( tmp );
//		return dl;
//	}

	/**
	 * @param devType1: Inflow, Outflow, Output, City, Zone
	 * @param devType2: Inflow, Outflow, Output, City, Zone
	 * @return DeviceList with a subset of the initial devices
	 */
//	public DeviceList getSubDevList(String devType1, String devType2, String devType3) {
//		DeviceList dl = getSubDevList(devType1, devType2);
//		dl.getDeviceList().addAll(getDevListPerDevType(devType3));
//		return dl;
//	}

	/**
	 * @return deviceList
	 */
	public ArrayList<Device> getDeviceList() {
		return deviceList;
	}

	/**
	 * @param deviceList
	 */
	public void setDeviceList(ArrayList<Device> deviceList) {
		this.deviceList=deviceList;
	}
	
	/**
	 * @return the names of device types
	 */
	public Set<String> getDeviceTypeSet() {
		Set<String> dtypeSet = new HashSet<String>();
		for(Device d : this.deviceList)
			dtypeSet.add(d.getType());
		return dtypeSet;
	}

//	/**
//	 * @return the names of dmas
//	 */
//	public Set<String> getdmaSet() {
//		Set<String> dmaSet = new HashSet<String>();
//		for(Device d : this.deviceList)
//			if (d.getType().equals(DeviceTypeInflow) || d.getType().equals(DeviceTypeOutflow) || d.getType().equals(DeviceTypeOutput))
//				dmaSet.add(d.getDma());
//		return dmaSet;
//	}

	/**
	 * @return the names of customer types
	 */
//	public Set<String> getCustTypeSet() {
//		Set<String> ctSet = new HashSet<String>();
//		for(Device d : this.deviceList)
//			if (d.getType().equals(DeviceTypeOutput))
//				ctSet.add(d.getCustomerType());
//		return ctSet;
//	}

	/**
	 * @return the names of meters
	 */
//	public Collection<? extends String> getMeterSet() {
//		Set<String> meterSet = new HashSet<String>();
//		for(Device d : this.deviceList)
//			if (d.getType().equals(DeviceTypeInflow) || d.getType().equals(DeviceTypeOutflow))
//				meterSet.add(d.getName());
//		return meterSet;
//	}

	/**
	 * @return the number of meters
	 */
//	public int getMeterCount() {
//		return getMeterSet().size();
//	}

	/**
	 * @return the number of dmas
	 */
//	public int getDmaCount() {
//		return getdmaSet().size();
//	}

	/**
	 * @return the number of customer types
	 */
//	public int getCustomerTypeCount() {
//		return getCustTypeSet().size();
//	}

	/**
	 * @return the number of device types
	 */
	public int getDeviceTypeCount() {
		return getDeviceTypeSet().size();
	}

	/**
	 * @param dma
	 * @return set of devices belonging to this dma
	 */
	public ArrayList<Device> getDevListPerDMA(String dma) {
		ArrayList<Device> devList = new ArrayList<Device>();
		for( Device dev : this.deviceList)
			if (dev.getDeviceID().equals(dma))
				devList.add(dev);
		return devList;
	}

	/**
	 * @param custType
	 * @param dmaSelectedSet 
	 * @return set of devices having this customer type 
	 */
//	public ArrayList<Device> getDevListPerCustType(String custType, Set<String> dmaSelectedSet) {
//		ArrayList<Device> devList = new ArrayList<Device>();
//		for( Device dev : this.deviceList)
//			if (dev.getCustomerType().equals(custType) && dmaSelectedSet.contains(dev.getDma()))
//				devList.add(dev);
//		return devList;
//	}





//	/**
//	 * @param devType
//	 * @return set of devices having this device type
//	 */
	public ArrayList<Device> getDevListPerDevId(String dmaSelected) {
		ArrayList<Device> devList = new ArrayList<Device>();
		for( Device dev : this.deviceList)
			if (dev.getDeviceID().equals(dmaSelected))
				devList.add(dev);
		return devList;
	}

	
	
	
	////////////////TEMPORARY STORAGE/////////////////////////
	
//	public DeviceList(String devType) {
//		deviceList = new ArrayList<Device>(Services.getDevicesByType(devType));
//	}
//
//	public DeviceList(String devType1, String devType2) {
//		ArrayList<Device> deviceList1 = new ArrayList<Device>(Services.getDevicesByType(devType1));
//		ArrayList<Device> deviceList2 = new ArrayList<Device>(Services.getDevicesByType(devType2));
//		deviceList1.addAll(deviceList2);
//		deviceList=deviceList1;		
//	}
//

}
