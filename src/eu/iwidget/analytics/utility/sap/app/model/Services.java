package eu.iwidget.analytics.utility.sap.app.model;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.ReadablePeriod;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ibm.icu.impl.duration.TimeUnit;
import com.ibm.icu.text.SimpleDateFormat;
import com.vaadin.data.Item;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;

import eu.iwidget.analytics.utility.sap.app.networkSimulation.NetworkLink;
import eu.iwidget.analytics.utility.sap.app.networkSimulation.NetworkNode;



public class Services {
	private static String basicURL;
	//	private static final String basicURL = "http://iriiwdg01.emerald-cloud.net:10039/";  //IP address mapping
	//	private static final String basicURL = "http://195.212.132.10:10039/";  //External IP address for the partners
	//	private static final String basicURL = "http://172.18.8.10:10039/";  //Internal IBM IP address
	public static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").withZoneUTC();
	//SAP uses a proxy that blocks big URLs, so we break the oversized HTTP calls in smaller ones for subsetSize Device subsets  
	private static final int subsetSize = 200; 
	private static Properties config = new Properties();
	private String proxy; //yes or no
	private boolean proxyServerUsed; //true if PROXY_SERVER = YES  in the settings.propoerties file
	private ArrayList<String> unitsFromServer = new ArrayList<String>();

	//Determines if a proxy is used
	public Services(){
		getProxyConf();
		getBasicURL();
	};

	private void getBasicURL() {
		try {
			config.load(this.getClass().getClassLoader().getResourceAsStream("settings.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		basicURL = config.getProperty("basicURL");
	}

	/**
	 * @return Arraylist of all devices
	 */
	public ArrayList<Device> getAllDevices() {
		String URL = basicURL + "iWidget/aggregation/";
		return getDeviceList(URL, "ALL");
	}

	/**
	 * @param URL
	 * @param devType: if devType=="ALL" return all devices, else only the specified devType
	 * @return
	 */
	private ArrayList<Device> getDeviceList(String URL, String devType) {
		String JSONString =getJson(URL);
		JSONArray deviceArray = null;
		ArrayList<Device> deviceList = new ArrayList<Device>();
		try{
			// for each JSON object in the array get the details
			deviceArray = new JSONArray(JSONString);
			for(int i=0;i<deviceArray.length();i++){
				JSONObject devJson = deviceArray.getJSONObject(i);
				String type = devJson.getString("type");
				String value = devJson.getString("value");
				String period = devJson.getString("period");
				String deviceID = devJson.getString("deviceID");
				String date = devJson.getString("date");
				String units = devJson.getString("units");
				Device dev=new Device(deviceID, type, period, value, date, units);
				deviceList.add(dev);
			}
		}catch (JSONException e){
			e.printStackTrace();
		}
		return deviceList;
	}


	public Map<String, Double> calcPercentile (String timeSerSel, String dmaSelected, String period, String startTime, String endTime){
		String units = "cms";
		Map<String, Map<String, Double>> mapa=new TreeMap<String, Map<String, Double>>();

		Map<String, Double> zaPoslat = new LinkedHashMap<String, Double>();
		Map<String, Double> inner = new TreeMap<String, Double>();
		Map<String, Double> inPerc = new TreeMap<String, Double>();

		if ("Real losses".equalsIgnoreCase(timeSerSel)){
			inner=(getRealLoss(timeSerSel,dmaSelected, period,  startTime, endTime,  units));
		}
		else if ("Inflow".equalsIgnoreCase(timeSerSel)){
			inner=(getInflow(timeSerSel,dmaSelected, period,  startTime, endTime,  units));
		}
		else if ("Billed Metered Consumption".equalsIgnoreCase(timeSerSel)){
			inner=(getBMC(timeSerSel,dmaSelected,period, startTime, endTime, units));
		}
		else {
			inner=getTimeSeries(period, dmaSelected, startTime, endTime, timeSerSel, units);
		}

		//inner=getTimeSeries(period, dmaSelected, startTime, endTime, timeSerSel, units);

		double [] array = new double[inner.size()];
		int count = 0;
		Iterator it = inner.entrySet().iterator();

		while (it.hasNext()){
			Map.Entry ulaz = (Map.Entry)it.next();
			String date = (String) ulaz.getKey();
			Double value = (Double) ulaz.getValue();
			array[count]= value;
			count++;
		}

		Arrays.sort(array);

		double high = 95.0;
		double open = 75.0;
		double close = 25.0;
		double low = 5.0;

		Percentile perc = new Percentile();
		Percentile pHigh = new Percentile(high);
		Percentile pOpen = new Percentile(open);
		Percentile pClose = new Percentile(close);
		Percentile pLow = new Percentile(low);
		double valMid = perc.evaluate(array);
		double valHigh = pHigh.evaluate(array);
		double valOpen = pOpen.evaluate(array);
		double valClose = pClose.evaluate(array);
		double valLow = pLow.evaluate(array);

		zaPoslat.put("open", valOpen);
		zaPoslat.put("close", valClose);
		zaPoslat.put("high", valHigh);
		zaPoslat.put("low", valLow);
		zaPoslat.put("mid", valMid);

		return zaPoslat;
	}


	public Map<String, Double> getInflow (String timeSerSel, String dmaSelected,
			String period, String startTime, String endTime, String units){

		String meterID;
		Map<String, Double> innermap = new TreeMap<String, Double>();
		Map<String, Double> innermap2 = new TreeMap<String, Double>();
		Map<String, Double> innermap3 = new TreeMap<String, Double>();


		if (("DMA 2").equals(dmaSelected)){
			innermap2 = getTimeSeries(period, "9336", startTime, endTime, timeSerSel, units);
			innermap3 = getTimeSeries(period, "9277", startTime, endTime, timeSerSel, units);
			Double residual;
			Iterator it = innermap2.entrySet().iterator();
			Iterator it2 = innermap3.entrySet().iterator();


			while (it.hasNext() && it2.hasNext()) {
				Map.Entry mc6 = (Map.Entry)it.next();
				Map.Entry mc9 = (Map.Entry)it2.next();
				String date = (String) mc6.getKey();
				Double mc6Value = (Double) mc6.getValue();

				Double mc9Value = (Double) mc9.getValue();
				residual = mc6Value-mc9Value;
				innermap = mapNewEntry (innermap, date, residual);
			}
		}

		else if(("DMA 1").equals(dmaSelected)){ 
			meterID="9305";
			innermap = getTimeSeries(period, meterID, startTime, endTime, timeSerSel, units);
		}

		else{ 
			meterID = "9283";
			innermap = getTimeSeries(period, meterID, startTime, endTime, timeSerSel, units);
		}

		return innermap;
	}

	public Map<String, Double> getBMC(String timeSerSel, String dma,
			String period, String from, String to, String units) {
		
		JSONArray response = getJsonArray (dma, from, to, period, timeSerSel);
		Map<String, Double> map = new TreeMap<String, Double>();
		if(response!=null){
			for(int i=0;i<response.length();i++){
				JSONObject dataPoints = null;
				String date = null;
				try {
					dataPoints = response.getJSONObject(i);
					date = dataPoints.getString("date");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Notification.show("Some data is not available in the selected time period!");
					e.printStackTrace();
				}
				if(isBefore(date, from)) continue;
				if(!isBefore(date, to)) continue;
				
				/* we only get here if date is important */
				String dateForm = null;
				Double value = null;
				try {
					dateForm = getFormattedDate(period, dataPoints.getString("date"));
					value = nonNegative(dataPoints, timeSerSel);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if ((dateForm != null) && (value != null))
					map = mapNewEntry(map , dateForm, value);
			}}
		
		return map;
	} 
	
			
	/**
	 * @param date 
	 * @param reference
	 * @return true if date is earlier than reference, false otherwise.
	 */
	private boolean isBefore(String date, String reference) {
		int dateYear = Integer.parseInt(date.substring(0,4));
		int referenceYear = Integer.parseInt(reference.substring(0,4));
		if(dateYear < referenceYear) return true;
		if(dateYear > referenceYear) return false;
		
		int dateMonth = Integer.parseInt(date.substring(5,7));
		int referenceMonth = Integer.parseInt(reference.substring(5,7));
		if(dateMonth < referenceMonth) return true;
		if(dateMonth > referenceMonth) return false;
		
		int dateDay = Integer.parseInt(date.substring(8,10));
		int referenceDay = Integer.parseInt(reference.substring(8,10));
		if(dateDay < referenceDay) return true;
		if(dateDay > referenceDay) return false;
		
		int dateHour = Integer.parseInt(date.substring(11,13));
		int referenceHour = Integer.parseInt(reference.substring(11,13));
		if(dateHour < referenceHour) return true;
		if(dateHour > referenceHour) return false;
		
		int dateMinute = Integer.parseInt(date.substring(14,16));
		int referenceMinute = Integer.parseInt(reference.substring(14,16));
		if(dateMinute < referenceMinute) return true;
		if(dateMinute > referenceMinute) return false;
		
		int dateSecond = Integer.parseInt(date.substring(17,19));
		int referenceSecond = Integer.parseInt(reference.substring(14,16));
		if(dateSecond < referenceSecond) return true;
		if(dateSecond > referenceSecond) return false;
		
		return false;
	}

	public Map<String, Double> getRealLoss (String timeSerSel, String dmaSelected,
			String period, String startTime, String endTime, String units){

		Map<String, Double> innermap = new TreeMap<String, Double>();
		Map<String, Double> innermap2 = new TreeMap<String, Double>();
		Map<String, Double> mapSum = new TreeMap<String, Double>();

		innermap = getTimeSeries(period, dmaSelected, startTime, endTime, "Leakage", units);
		innermap2 = getTimeSeries(period, dmaSelected, startTime, endTime, "PipeBurst", units);

		Double sum;
		Iterator it = innermap.entrySet().iterator();
		Iterator it2 = innermap2.entrySet().iterator();


		while (it.hasNext() && it2.hasNext()) {
			Map.Entry leak = (Map.Entry)it.next();
			Map.Entry pb = (Map.Entry)it2.next();
			String date = (String) leak.getKey();
			Double vleak = (Double) leak.getValue();

			Double vpb = (Double) pb.getValue();
			sum = vleak+vpb;
			mapSum = mapNewEntry (mapSum, date, sum);
		}

		return mapSum;
	}


	public Map<String, Double> getTimeSeries(String resolution, String deviceID, 
			String startTime, String endTime, String type, String units){


		Map<String, Double>  Mapa = new TreeMap<String, Double>();

		try {
			JSONArray array = getJsonArray (deviceID, startTime, endTime, resolution, type);
			if (array!=null){
				if ("minPressure".equals(type)){
					JSONObject object = array.getJSONObject(0);
					String min = object.getString("minPressure");
					Double minPress = Double.valueOf(min);
					Mapa.put("Minimum Pressure Value", (minPress/1000));
				}
				else{

					//JSONArray devIdSeries = array.getJSONObject(j).getJSONArray("timeSeries");
					JSONObject timeSer = array.getJSONObject(0);
					if (StringUtils.isNotEmpty(timeSer.getString("timeSeries"))){
						String ts = timeSer.getString("timeSeries");
						
						if(StringUtils.isNotEmpty(timeSer.getString("units"))){
							this.unitsFromServer.add(timeSer.getString("units"));
						}
						
						JSONArray array2 = new JSONArray(ts);
						if(array2!=null){
							for(int i=0;i<array2.length();i++){
								JSONObject timeSeries = array2.getJSONObject(i);
								String dateForm=getFormattedDate(resolution, timeSeries.getString("date"));
								Double value = nonNegative(timeSeries, type);
								//Set new key:value for map<dma:value>
								//dmaVal = mapNewEntry(dmaVal, dma, value);
								//Set new key:value for map<date:value>
								if ((dateForm != null) && (value != null))
									Mapa = mapNewEntry(Mapa , dateForm, value);
							}}}}}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		return Mapa;	
	}				



	/**
	 * Create a new map entry, or increase existing value of date by newValue
	 * @param map
	 * @param date
	 * @param newValue
	 * @return
	 */
	Map<String, Double> mapNewEntry(Map<String, Double> map, String date, Double newValue) {
		if (map.get(date)==null)
			map.put(date, newValue);
		else
			map.put(date, Double.valueOf(map.get(date).doubleValue()+newValue));
		return map;
	}

	/**
	 * Returns the Json file with all the requested entries.
	 * If a proxy is used, set proxy host and proxy port.
	 * @param URL
	 * @return String representing a JSONArray
	 */
	public String getJson(String URL) {
		// setup the Apache Wink Client
		RestClient client;
		if (proxyServerUsed) {
			client = new RestClient(new ClientConfig().proxyHost("proxy").proxyPort(8080));
		}
		else{
			client = new RestClient(new ClientConfig());
		}

		// point the client to the desired REST URL
		Resource resource = client.resource(URL);

		// make sure the result comes back as a JSON string
		resource.accept("application/json");

		if(URL==null)
			System.out.println("NULL url");

		//print the url
		System.out.println(URL);

		// invoke the REST URL
		ClientResponse response = resource.get();

		// do some checks on what we get back
		// DEBUG print the response code
		//        System.out.println("The Response Code is: " + response.getStatusCode());

		// need better error checking here and should throw an exception
		if (response.getStatusCode() != 200) {
			System.out.println("The Received Error is: " + response.getEntity(String.class));
			return null;
		}

		return response.getEntity(String.class);
	}


	public Double getAverage (Map<String, Double> innermap){
		//Map<String, Double> map = new TreeMap<String, Double>();
		Iterator it = innermap.entrySet().iterator();
		Double value;
		int count = 0;
		Double average;
		Double sum = new Double(0);
		while (it.hasNext()) {
			Map.Entry el = (Map.Entry)it.next();
			value = (Double) el.getValue();
			sum=sum+value;
			count++;
		}
		average= sum/count;
		return average;
	}

	/**
	 * get proxy configuration from settings.properties file
	 */
	private void getProxyConf() {
		try {
			config.load(this.getClass().getClassLoader().getResourceAsStream("settings.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		proxy = config.getProperty("PROXY_SERVER");

		//set up proxyServerUsed
		if(proxy.equals("YES"))
			proxyServerUsed=true;
		else
			proxyServerUsed=false;
	}

	/**
	 * @param deviceId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	private JSONArray getJsonArray(String dmaSelected, String startTime, String endTime,String period,String type) {
		String URL = getSingleTimeSeriesURL(dmaSelected, startTime,endTime, period, type);
		String JSONString = getJson(URL);

		if (JSONString==null)
			return null;
		JSONArray deviceArray = null;
		try {
			deviceArray = new JSONArray(JSONString);
		} catch (JSONException e) {
			Notification.show("Some data is not available in the selected time period!");
		}
		return deviceArray;
	}

	/**
	 * @param URL
	 * @return JSONArray from URL
	 */
	private JSONArray getJsonArray(ArrayList<Device> deviceList, String startTime, String endTime) {
		ArrayList<ArrayList<Device>> deviceSets = getSubsets(deviceList);
		JSONArray devArraySum = new JSONArray();
		for (ArrayList<Device> dl : deviceSets) {
			/*????*/	String URL = basicURL + "iWidget/aggregation";
			String JSONString = getJson(URL);
			if (JSONString == null)
				return null;
			JSONArray deviceArray = null;
			try {
				deviceArray = new JSONArray(JSONString);
			} catch (JSONException e) {
				Notification.show("Some data is not available in the selected time period!");
			}
			devArraySum = concatJsonArrays(devArraySum, deviceArray);
		}
		return devArraySum;
	}

	/**
	 * @param devList
	 * @return
	 */
	private static ArrayList<ArrayList<Device>> getSubsets(ArrayList<Device> devList) {
		int devListSize = devList.size();
		ArrayList<ArrayList<Device>> subsets = new ArrayList<ArrayList<Device>>();
		for (int i=0 ; i<devListSize ; i+= (i+subsetSize<devListSize)? subsetSize : devListSize%subsetSize){
			subsets.add(getSet(i, devList));
		}
		return subsets;
	}

	/**
	 * @param start
	 * @param devList
	 * @return a subset of max size divisionSize from devList starting from index start
	 */
	private static ArrayList<Device> getSet(int start,    List<Device> devList) {
		HashSet<Device> devListPart = new HashSet<Device>();
		for(int j=start ; j<start+subsetSize && j<devList.size(); j++){
			devListPart.add(devList.get(j));
		}
		return new ArrayList<Device>(devListPart);
	}

	/**
	 * @param devList
	 * @param startTime
	 * @param endTime
	 * @return shape the group time series url
	 */
	//	private static String getGroupTimeSeriesURL(ArrayList<Device> devList, String startTime, String endTime) {
	//		ArrayList<String> devIds = new ArrayList<String>(getDevIds(devList));
	//		String URL=basicURL + "iWidget/timeSeries/group/[";
	//		for(String deviceId : devIds){
	//			URL += "{\"id\":\"" + deviceId + "\"},";
	//		}
	//		URL = removeLastChar(URL);
	//		URL += "]?startTime="+startTime+"&endTime="+endTime;
	//		URL = urlencoding(URL);
	//		return URL;
	//	}



	private static String getSingleTimeSeriesURL(String dmaSelected,String startTime,String endTime, String period,String type) {
		String meterID;
		String URL;
		if("Pressure".equalsIgnoreCase(type)){
			if ("DMA 1".equalsIgnoreCase(dmaSelected)) meterID="9296";
			else {meterID="9340";}
			if("hourly".equalsIgnoreCase(period)){
				URL = basicURL + "iWidget/timeSeries/"+meterID+"/pressure/hourly/downstream?startTime="+startTime+"&endTime="+endTime;
			}
			else{
				URL = basicURL + "iWidget/timeSeries/"+meterID+"/pressure/daily/downstream?startTime="+startTime+"&endTime="+endTime;
			}
		}
		else if("Pipe Bursts".equalsIgnoreCase(type)){
			URL = basicURL + "iWidget/aggregation/PipeBurst/"+period+"/"+dmaSelected+"/timeSeries?startTime="+startTime+"&endTime="+endTime;
		}
		else if("Inflow".equalsIgnoreCase(type)){
			URL = basicURL + "iWidget/timeSeries/"+dmaSelected+"?startTime="+startTime+"&endTime="+endTime;
		}
		else if("minPressure".equalsIgnoreCase(type)){
			URL = basicURL + "iWidget/device/"+dmaSelected+"/minPressure";
		}
		else if("Billed Metered Consumption".equalsIgnoreCase(type)){
			URL = basicURL + "iWidget/aggregation/Billed%20metered%20consumption/"+period+"/"+dmaSelected;
		}
		else{
			URL = basicURL + "iWidget/aggregation/"+type+"/"+period+"/"+dmaSelected+"/timeSeries?startTime="+startTime+"&endTime="+endTime;
		}
		return urlencoding(URL);
	}	

	/**
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	private static JSONArray concatJsonArrays(JSONArray arr1, JSONArray arr2) {
		JSONArray result = new JSONArray();
		try {
			for (int i = 0; i < arr1.length(); i++) {
				result.put(arr1.get(i));
			}
			for (int i = 0; i < arr2.length(); i++) {
				result.put(arr2.get(i));
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * @param res: Monthly, Daily, Weekly, Hourly, 15 Min, Quarterly, 12 Months
	 * @param dateStr
	 * @return formatted date in form of a string
	 */
	public static String getFormattedDate(String res, String dateStr) {
		if ((res!= null) && (dateStr != null)){
			//		if(res.equals("Monthly")){
			//			return dateStr.substring(0,7);
			//		}
			/*	else */if((res.equals("Daily")) || (res.equals("daily"))){
				return dateStr.substring(0,10);
			}
			//		else if(res.equals("Weekly")){
			//			return "CW"+String.valueOf(getDateTime(dateStr).getWeekOfWeekyear());
			//		}
			else if((res.equals("Hourly")) || (res.equals("hourly"))){
				return dateStr.substring(5,16);
			}
			//		else if(res.equals("15 Min")){
			//			return dateStr.substring(5,16);
			//		}
			//		else if(res.equals("Quarterly")){
			//			if( getDateTime(dateStr).getMonthOfYear() <=3 )
			//				return "Q1";
			//			if( getDateTime(dateStr).getMonthOfYear() <=6 )
			//				return "Q2";
			//			if( getDateTime(dateStr).getMonthOfYear() <=9 )
			//				return "Q3";
			//			if( getDateTime(dateStr).getMonthOfYear() <=12 )
			//				return "Q4";
			//		}
			//		else if(res.equals("12 Months"))
			//			return getDateTime(dateStr).toString(FORMATTER_MONTH);
		}
		return null;
	}



	public static String getDateOfValue(String res, LocalDate time, String startTime){
		String DateOfValue=null;
		if(res.equals("Monthly")){
			DateOfValue= time+startTime.substring(7,19);
			return DateOfValue;
		}
		else if((res.equals("Daily")) || (res.equals("daily"))){
			DateOfValue= time+startTime.substring(10,19);
			return DateOfValue;
		}
		else if(res.equals("Weekly")){
			DateOfValue= time+startTime.substring(7,19);
			return DateOfValue;
		}
		else if((res.equals("Hourly")) || (res.equals("hourly"))){
			DateOfValue= startTime.substring(0,5)+time+startTime.substring(16,19);
			return DateOfValue;
		}

		return null;
	}

	/**
	 * gets a valid date time
	 * @param dt the String to be parsed into a valid time format
	 * @return a DateTime object
	 */
	public static DateTime getDateTime(String dtS){
		return FORMATTER.parseDateTime(dtS);
	}

	/**
	 * Get the non-negative value from the time series
	 * @param timeSeries
	 * @return 
	 */
	private Double nonNegative(JSONObject timeSeries, String type) {
		Double value = 0.0;
		try {
			if("Pressure".equals(type))
				value = (timeSeries.getDouble("value") < 0) ? 0.0 : (timeSeries.getDouble("value")/1000); /* Pa to kPa */
			//else value = (timeSeries.getDouble("value") < 0) ? 0.0 : SECONDS_PER_HOUR *  timeSeries.getDouble("value");
			else value = (timeSeries.getDouble("value") < 0) ? 0.0 : timeSeries.getDouble("value");


		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}

	/**Add dmaVal map and dateVal map to list
	 * @param dmaVal
	 * @param dateVal
	 * @return
	 */
	List<Map<String, Double>> list( Map<String, Double> dateVal) {
		List<Map<String, Double>> map = new ArrayList<Map<String, Double>>();
		map.add(dateVal);
		return map;
	}

	/**
	 * @param devList
	 * @param id
	 * @return
	 */
	//	private static String getDmaFromDevId(ArrayList<Device> devList, String id) {
	//		for (Device dev : devList)
	//			if (dev.getId().equals(id))
	//				return dev.getDma();
	//		return null;
	//	}

	/**
	 * @param devList
	 * @return
	 */
	//	private static ArrayList<String> getDevIds(ArrayList<Device> devList) {
	//		ArrayList<String> deviceIds = new ArrayList<String>(); 
	//		for (Device dev : devList){
	//			deviceIds.add(dev.getId());
	//		}
	//		return deviceIds;
	//	}

	/**
	 * replace <<space [ ] { } ">> with their url encoding
	 * @param url not encoded
	 * @return encoded url
	 */
	private static String urlencoding(String url) {
		url = url.replaceAll(" ", "%20");
		url = url.replaceAll("\\[", "%5B");
		url = url.replaceAll("\\]", "%5D");
		url = url.replaceAll("\\{", "%7B");
		url = url.replaceAll("\\}", "%7D");
		url = url.replaceAll("\"", "%22");
		return url;
	}

	/**
	 * @param str
	 * @return
	 */
	private static String removeLastChar(String str) {
		return str.substring(0,str.length()-1);
	}

	@SuppressWarnings("unchecked")
	public TableWarning[] fillTableData(String[] selectedFilter){
		
		//in case any of the fields are null, just return empty array right now
		for(String s : selectedFilter){
			if(s == null) return new TableWarning[0];
		}
		
		String networkSector = selectedFilter[0];
		String tolerance = selectedFilter[1];
		String startTime = selectedFilter[2];
		String endTime = selectedFilter[3];

		String jsonFromApi = getJson(urlencoding(basicURL+"iWidget/tree/" + networkSector + 
				"/min/" + tolerance + "?startTime=" + startTime + "&endTime=" + endTime));
		
		//if there are no results, return empty array right now
		if(jsonFromApi == null) { 
			Notification.show("There are no results for this period!");
			return new TableWarning[0];
		}
		
		JSONArray deviceDataPoint = null;
		ArrayList<DeviceDataPoint> dataPointList = new ArrayList<DeviceDataPoint>();
		try{
			deviceDataPoint = new JSONArray(jsonFromApi);
			
			//if there are no results, just return empty array right now
			if(deviceDataPoint.length() == 0) { 
				Notification.show("There are no results for this period!");
				return new TableWarning[0];
			}
			
			
			// for each JSON object in the array get the details
			for(int i = 0; i < deviceDataPoint.length(); i++){
				JSONObject devJson = deviceDataPoint.getJSONObject(i);
				Double value = devJson.getDouble("value");
				String deviceID = devJson.getString("deviceID");
				String date = devJson.getString("date");
				Integer count = devJson.getInt("count");				
				DeviceDataPoint d = new DeviceDataPoint(value, deviceID, date, count);
				dataPointList.add(d);
			}
		}catch (JSONException e){
			e.printStackTrace();
		}	
		
		String relevantDate = subtractToDate(endTime, 1);
		//creating a list with every result that has endTime - 1 date.
		ArrayList<TableWarning> warningsList = new ArrayList<TableWarning>();
        for(DeviceDataPoint dp: dataPointList){
            if(areDatesEqual(dp.date, relevantDate) &&
            		dp.deviceID.length() > 4){
                warningsList.add(new TableWarning(dp.deviceID, 1, dp.value));
            }
        }
        
        if(warningsList.isEmpty()) { 
			Notification.show("There are no warnings for this period!");
			return new TableWarning[0];
		}
        
        //checks every day in the provided interval for the relevant devices, updating data like "leaking for"
        for(int i=2;i <= daysBetween(startTime, endTime); i++){
            String date = subtractToDate(endTime, i);
            
            for(TableWarning tw : warningsList){
                if(tw.isRunningLeakage()){
                    Double minValue = idExistsInDatapoints(tw, dataPointList, date);
                    if (minValue < 0){ //minValue = -1, meaning no leakage observed in this day for this device
                    	tw.setRunningLeakage(false);
                    }
                    else {
                        //Leakage observed in this day
                        tw.setLeakingFor(tw.getLeakingFor() + 1);
                        tw.getMinimumValues().add(minValue);
                    }
                }
            }    
        }
        
        for(TableWarning tw : warningsList){
        	if(!tw.getMinimumValues().isEmpty())
        	tw.setMinimumValue(Collections.min(tw.getMinimumValues()));
        }
        TableWarning[] tw = new TableWarning[warningsList.size()];
        //Notification.show("size of TableWarnings list: " + warningsList.size());
        return warningsList.toArray(tw);
	}


	/**
	 * @param date1 A date as a String.
	 * @param date2 Another date as a String.
	 * @return true if date1 and date2 represent the same day, month and year, false otherwise.
	 */
	private boolean areDatesEqual(String date1, String date2){
		String date1Formatted = date1.substring(0,10);
		String date2Formatted = date2.substring(0,10);
		return date1Formatted.equals(date2Formatted);
	}


	/**
	 * @param startTime The first day as a string.
	 * @param endTime The second day as a string.
	 * @return the number of days between startTime and endTime.
	 */
	private int daysBetween(String startTime, String endTime){

		int diffDays = 0;
		try {
			String startTimeFormatted = startTime.substring(0,10);
			String endTimeFormatted = endTime.substring(0,10);
			java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
			Date startObj = dateFormat.parse(startTimeFormatted);
			Date endObj = dateFormat.parse(endTimeFormatted);
			
			long diff = endObj.getTime() - startObj.getTime();
			diffDays = (int) java.util.concurrent.TimeUnit.DAYS.convert(diff, java.util.concurrent.TimeUnit.MILLISECONDS);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return diffDays;		
	}

	/**
	 * @param datetime A date in String type.
	 * @param interval Number of days to subtract to date
	 * @return The new date, after subtracting interval to datetime, as a String.
	 */
	private String subtractToDate(String datetime, int interval){

		try {
			String dateString = datetime.substring(0,10);
			java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
			Date dateObj = dateFormat.parse(dateString);

			Calendar c = Calendar.getInstance();
			c.setTime(dateObj);
			c.add(Calendar.DATE, (-1 * interval));

			dateObj.setTime(c.getTime().getTime());

			//subtract 'interval days' in miliseconds
			//long dateUnix = dateObj.getTime();
			//dateObj.setTime(dateUnix - (interval * 24 * 3600 * 1000));

			return dateFormat.format(dateObj);            

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}    

    /**
     * This methods checks if a given tableWarning exists in the parsed datapoints (by id), on a specific date.
     * @param tw warning to be checked.
     * @param datapoints list of datapoints from database.
     * @param date date to be checked on.
     * @return the minimum leakage value for that day and object, or -1 if such record doesn't exist.
     */
    private Double idExistsInDatapoints(TableWarning tw, ArrayList<DeviceDataPoint> datapoints, String date){
        
        for(DeviceDataPoint dp : datapoints){
            if(dp.deviceID.equals(tw.getDeviceId()) && 
                    dp.date.substring(0,10).equals(date)){
                return dp.value;
            }
        }
        return -1D;
    }

    public ArrayList<String> getUnitsFromServer() {
    	return this.unitsFromServer;
    }
    
    public void setUnitsFromServer(ArrayList<String> s){
    	this.unitsFromServer = s;
    }
    
    /**
     * @author diogofranco
     * @param node - A networkNode instance 
     * (represents a line of .nodes.out file after weighted average)
     * @param hMin - The minimum pressure chosen by the user
     * @return A grade from 0-3 to plot later.
     */
    public static Double gradeMinPressure(NetworkNode node, Double hMin){
    	Double p = node.getPressure();
    	if(p <= 0.75 * hMin) return 0D;
    	else if(p < hMin) return ((12 / hMin) * p) - 9;
    	
    	return 3D;
    }
    
    /**
     * @author diogofranco
     * @param node - A networkNode instance 
     * (represents a line of .nodes.out file after weighted average)
     * @param hMin - The minimum pressure chosen by the user
     * @param hMax - The maximum pressure chosen by the user
     * @return A grade from 0-3 to plot later.
     */
    public static Double gradeMaxPressure(NetworkNode node, Double hMin, Double hMax){
    	Double p = node.getPressure();
    	if(p <= hMin) return 3D;
    	else if(p <= hMax) return ((-0.5 / (hMax - hMin)) * p + 3);
    	else if(p <= (1.5 * hMax)) return (6.5 - ((4 / hMax) * p));
    	else if(p <= (3 * hMax)) return (1 - (p / (3 * hMax)));
    	
    	return 0D;
    }
    
    /**
     * @author diogofranco
     * @param node - A networkLink instance 
     * (represents a line of .links.out file after weighted average)
     * @param vRef - The minimum velocity chosen by the user
     * @return A grade from 0-3 to plot later.
     */
    public static Double gradeMinVelocity(NetworkLink node, Double vRef){
    	Double p = node.getVelocity();
    	if(p <= 0.5 * vRef) return 0D;
    	else if(p < vRef) return ((6 / vRef) * p) - 3;
    	
    	return 3D;
    }

	public String getComplaintsResult(String[] selectedFilter) {
		String clientId = selectedFilter[0];
		String defaultMessage = "There are no warnings for client with id \""
				+ selectedFilter[0] + "\" on " + selectedFilter[1] +
				". Please examine this complaint further.";
		/*String Faults = getJson(urlencoding(basicURL+"iWidget/aggregation/Warning/Fault/" 
				+ clientId));*/
		String Faults = getJson(urlencoding(basicURL+"iWidget/aggregation/Warning/Fault/" 
				+ clientId));
		
		/*String Size = getJson(urlencoding(basicURL+"iWidget/aggregation/Warning/Size/" 
				+ clientId));*/
		String Size = getJson(urlencoding(basicURL+"iWidget/aggregation/Warning/Size/" 
				+ clientId));
		
		//if there are no results, return with no warnings now
		if(Faults == null && Size == null) { 
			return defaultMessage;
		}
		
		JSONArray faultDatapoints = null;
		if(Faults != null){
			try {
				faultDatapoints = new JSONArray(Faults);

				for(int i = 0; i < faultDatapoints.length(); i++) { 
					JSONObject devJson = faultDatapoints.getJSONObject(i);
					String date = devJson.getString("date");

					if(areDatesEqual(date,selectedFilter[1])){
						String message = devJson.getString("message");
						return "A fault in the network occurred for client with id \"" +
						clientId + "\" on " + selectedFilter[1] + ":\n\"" + message +
						"\".";
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		if(Size!=null){
			JSONArray sizeDatapoints = null;
			try {
				sizeDatapoints = new JSONArray(Size);

				for(int i = 0; i < sizeDatapoints.length(); i++) { 
					JSONObject devJson = sizeDatapoints.getJSONObject(i);
					String date = devJson.getString("date");

					if(areDatesEqual(date,selectedFilter[1])){
						String message = devJson.getString("message");
						return "Possible inadequacy of water meter for client with id \"" +
						clientId + "\" on " + selectedFilter[1] + ":\n\"" + message +
						"\".";
					}
				}
			} catch(JSONException e) {
				e.printStackTrace();
			}
		}
		return defaultMessage;
	}
}
