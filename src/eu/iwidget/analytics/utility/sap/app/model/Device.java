package eu.iwidget.analytics.utility.sap.app.model;



public class Device {
	private String deviceID;
	private String type;
	private String period;
	private String value;
	private String date;
	private String units;

	
	public Device(String deviceID, String type, String period, String value, String timestamp, String units) {
		this.deviceID=deviceID;
		this.type=type;
		this.period=period;
		this.value=value;
		this.date=date;
		this.units=units;
	}
	
//	public Device(String period, String deviceId, String type, String timestamp, String units) {
//		this.deviceId=deviceId;
//		this.type=type;
//		this.period=period;
//		this.timestamp=timestamp;
//		this.units=units;
//	}

	public String getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value = value;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUnits() {
		return units;
	}

	public void setUnist(String units) {
		this.units = units;
	}

	

}
