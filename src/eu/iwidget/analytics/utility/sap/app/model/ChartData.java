package eu.iwidget.analytics.utility.sap.app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Observable;

import org.joda.time.format.DateTimeFormatter;

import eu.iwidget.analytics.utility.sap.app.ChartType;

/**
 * container class for chart data
 * @author kristina_kirsten
 *
 */

public class ChartData extends Observable implements Serializable{
	private static final long serialVersionUID = 1L;
	//DomId
	public String domId;
	public String ldomId;
	
	//Header
	public String originalTitle;
	public String title;
	public String description;
	
	//Axis
	public String xAxisTitle;
	public String yAxisTitle;
	public String originalYAxisTitle;
	public String otheryAxisTitle;
	public String yAxisUnit;

	public DateTimeFormatter timeformat;
	
	//Tables
	public boolean legend;
	
	
	//Type
	public ChartType type;
	
	//Values
	public LinkedHashSet<String> categories;
	public Double[][] values;
	public String[] seriesNames;
	public ArrayList<String> unitsFromServer;
//	public Double[][] values2;
	
	/**
	 * update values and trigger state changed event
	 */
	public void setValues(){
		setChanged();             
		notifyObservers(this.values); 
//		notifyObservers(this.values2); 
	}
}
