package eu.iwidget.analytics.utility.sap.app.model;

import java.util.Map;

import eu.iwidget.analytics.utility.sap.app.FilterCategory;
import eu.iwidget.analytics.utility.sap.app.FilterType;

/**
 * container class for filter data
 * @author kristina_kirsten
 *
 */
public class FilterData{
	
	public String title;
	public FilterType type;
	public FilterCategory category;
	public Map<Integer, String> values;
	public Object defaultValue;
	public String id;
	
	/**
	 * 
	 * @param filters
	 * @param type
	 * @return true if filter data contains filter type, false otherwise
	 */
	public static boolean contains(FilterData[] filters, FilterType type){
		boolean value = false;
		for (int i = 0; i < filters.length; i++) {
			if (filters[i].type == type) value = true;
		}
		return value;
	}
}
