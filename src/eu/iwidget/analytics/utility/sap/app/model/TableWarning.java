package eu.iwidget.analytics.utility.sap.app.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * An instance of this class represents a warning that is shown in the table of the use case 5.3.
 * @author Diogo Franco
 */
@SuppressWarnings("serial")
public class TableWarning implements Serializable{
	

	private String deviceId;
    private Integer leakingFor;
    private Double minimumValue;
    private String severity;
    private boolean Shown;
    private boolean RunningLeakage;
    private ArrayList<Double> minimumValues;
    
    public TableWarning(String deviceId, Integer leakingFor, Double minimumValue){
        this.deviceId = deviceId;
        this.leakingFor = leakingFor;
        this.minimumValue = minimumValue;
        this.severity = "Medium";
        this.Shown = true;
        this.RunningLeakage = true;
        this.setMinimumValues(new ArrayList<Double>());
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the leakingFor
     */
    public Integer getLeakingFor() {
        return leakingFor;
    }

    /**
     * @param leakingFor the leakingFor to set
     */
    public void setLeakingFor(Integer leakingFor) {
        this.leakingFor = leakingFor;
    }

    /**
     * @return the minimumValue
     */
    public Double getMinimumValue() {
        return minimumValue;
    }

    /**
     * @param minimumValue the minimumValue to set
     */
    public void setMinimumValue(Double minimumValue) {
        this.minimumValue = minimumValue;
    }

    /**
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }

    /**
     * @return the Shown
     */
    public boolean isShown() {
        return Shown;
    }

    /**
     * @param Shown the Shown to set
     */
    public void setShown(boolean Shown) {
        this.Shown = Shown;
    }

    /**
     * @return the RunningLeakage
     */
    public boolean isRunningLeakage() {
        return RunningLeakage;
    }

    /**
     * @param RunningLeakage the RunningLeakage to set
     */
    public void setRunningLeakage(boolean RunningLeakage) {
        this.RunningLeakage = RunningLeakage;
    }

	public ArrayList<Double> getMinimumValues() {
		return minimumValues;
	}

	public void setMinimumValues(ArrayList<Double> minimumValues) {
		this.minimumValues = minimumValues;
	}
}
