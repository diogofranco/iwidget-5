package eu.iwidget.analytics.utility.sap.app.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import com.vaadin.ui.Notification;

import eu.iwidget.analytics.utility.sap.app.model.WidgetModel.DBData;


public class WidgetModelImplREST implements WidgetModel {

	ArrayList<Device> devRandomSet;
	Services services;
	
	private List<Map<String, Map<String, Double>>> maps;
	String units = "cms";
		
	/**
	 * The constructor calls the REST service for all the devices and stores it in "dl".
	 */
	public WidgetModelImplREST() {
		services = new Services();
	}
	
	public DBData[] firstTab (String timeSerSel,String dmaSelected,String period, String from, String to){
		
		maps = initMaps();
		
		Set <String> dmaSet = new HashSet<String>();
		
		if (("all DMAs").equalsIgnoreCase(dmaSelected)){
			dmaSet.add("DMA 1");
			dmaSet.add("DMA 2");
			dmaSet.add("DMA 3");
		}
		else{
			dmaSet.add(dmaSelected);
		}
		
		Map<String, Double> inner = new TreeMap<String, Double>();
		Map<String, Double> inner2 = new TreeMap<String, Double>();
		
		for (String dma : dmaSet){
			if ("Real losses".equalsIgnoreCase(timeSerSel)){
				inner=(services.getRealLoss(timeSerSel,dma, period,  from, to,  units));
			}
			else if ("Inflow".equalsIgnoreCase(timeSerSel)){
				inner=(services.getInflow(timeSerSel,dma, period,  from, to,  units));
			}
			else if ("Billed Metered Consumption".equalsIgnoreCase(timeSerSel)){
				inner=(services.getBMC(timeSerSel,dma,period, from, to, units));
			}
			else {
				inner=(services.getTimeSeries(period, dma, from, to, timeSerSel, units));
			}
			
			inner2=(services.getTimeSeries(period, dma, from, to, "Pressure", units));
					
			if (dma != null){
				maps.get(0).put(dma, inner);
				maps.get(1).put(dma, inner2);
			}
		
		}	
		return mapsToData(maps);
	}
	
	public DBData[] secondTab (String timeSerSel,String dmaSelected,String period, String from, String to){
		
		maps = initMaps();
		
		Set <String> dmaSet = new HashSet<String>();
		
		if (("all DMAs").equalsIgnoreCase(dmaSelected)){
			dmaSet.add("DMA 1");
			dmaSet.add("DMA 2");
			dmaSet.add("DMA 3");
		}
		else{
			dmaSet.add(dmaSelected);
		}
		
		Map<String, Double> inner = new TreeMap<String, Double>();
		Map<String, Double> innermap = new TreeMap<String, Double>();
		Map<String, Double> inmapAvVal = new TreeMap<String, Double>();
		Map<String, Double> minPress = new TreeMap<String, Double>();
		
		for (String dmat : dmaSet){
		
		Double averageVal;	
		if ("Real losses".equalsIgnoreCase(timeSerSel)){
			innermap=(services.getRealLoss(timeSerSel,dmat, period,  from, to,  units));
		}
		else if ("Inflow".equalsIgnoreCase(timeSerSel)){
			innermap=(services.getInflow(timeSerSel,dmat, period,  from, to,  units));
		}
		else if ("Billed Metered Consumption".equalsIgnoreCase(timeSerSel)){
			innermap=(services.getBMC(timeSerSel,dmat,period, from, to, units));
		}
		else {
			innermap=services.getTimeSeries(period, dmat, from, to, timeSerSel, units);
		}
		
		inner=services.calcPercentile( "Pressure", dmat, period, from, to);

		averageVal =  (services.getAverage(innermap));
		
		minPress = services.getTimeSeries(period, dmat, from, to, "minPressure", units);
		
		
		if (dmaSelected!=null ){
			inmapAvVal.put(dmat, averageVal);
			inner.putAll(minPress);
			maps.get(0).put("Average value",inmapAvVal);
			maps.get(1).put(dmat, inner);
		}
		}
		return mapsToData(maps);
	}
	
	
	// jpcoelho: commented out this part to change number of campaigns from 5 to 2
	
//	public DBData thirdTab (String timeSerSel,String dma, String period, String from1, String to1, String from2, String to2, String from3, String to3, String from4, String to4, String from5, String to5){
//		
//		Map<String, Map<String, Double>> map = new TreeMap<String,Map<String,Double>>();
//		Map<String, Double> inner = new TreeMap<String, Double>();
//		Map<String, Double> innerPress = new TreeMap<String, Double>();
//		Double valPress;		
//		List<String> startTime = new ArrayList<String>(5);
//		List<String> endTime = new ArrayList<String>(5);
//
//		startTime.add(from1);
//		startTime.add(from2);
//		startTime.add(from3);
//		startTime.add(from4);
//		startTime.add(from5);
//		endTime.add(to1);
//		endTime.add(to2);
//		endTime.add(to3);
//		endTime.add(to4);
//		endTime.add(to5);
//
//		for (int j=0; j<5; j++){
//			inner=services.calcPercentile( timeSerSel, dma, period, startTime.get(j), endTime.get(j));
//			if ("Real losses".equalsIgnoreCase(timeSerSel)){
//				innerPress=(services.getRealLoss(timeSerSel, dma, period,  startTime.get(j), endTime.get(j),  units));
//			}
//			else if ("Inflow".equalsIgnoreCase(timeSerSel)){
//				innerPress=(services.getInflow(timeSerSel, dma, period,  startTime.get(j), endTime.get(j),  units));
//			}
//			else {
//				innerPress=services.getTimeSeries(period, dma, startTime.get(j), endTime.get(j), timeSerSel, units);
//			}
//
//			valPress = (services.getAverage(innerPress));
//			if(valPress != null){
//				inner.put("Average Pressure Value ", valPress);
//			}
//			map.put("campaign"+ (j+1), inner);
//		}
//
//		return mapToData(map);
//	}
	

	// jpcoelho: code for 2 campaigns
	public DBData thirdTab (String timeSerSel,String dma, String period, String from1, String to1, String from2, String to2, String from3, String to3){
		
		Map<String, Map<String, Double>> map = new TreeMap<String,Map<String,Double>>();
		Map<String, Double> inner = new TreeMap<String, Double>();
		Map<String, Double> tempMap = new TreeMap<String,Double>();
		Map<String, Double> innerPress = new TreeMap<String, Double>();
		Double valPress;		
		List<String> startTime = new ArrayList<String>(3);
		List<String> endTime = new ArrayList<String>(3);

		startTime.add(from1);
		startTime.add(from2);
		startTime.add(from3);

		endTime.add(to1);
		endTime.add(to2);
		endTime.add(to3);


		for (int j=0; j<3; j++){
			inner=services.calcPercentile( timeSerSel, dma, period, startTime.get(j), endTime.get(j));
			if ("Real losses".equalsIgnoreCase(timeSerSel)){
				innerPress=(services.getRealLoss(timeSerSel, dma, period,  startTime.get(j), endTime.get(j),  units));
			}
			else if ("Inflow".equalsIgnoreCase(timeSerSel)){
				innerPress=(services.getInflow(timeSerSel, dma, period,  startTime.get(j), endTime.get(j),  units));
			}			
			else if ("Billed Metered Consumption".equalsIgnoreCase(timeSerSel)){
				innerPress=(services.getBMC(timeSerSel,dma,period, startTime.get(j), endTime.get(j), units));
			}
			else {
				innerPress=services.getTimeSeries(period, dma, startTime.get(j), endTime.get(j), timeSerSel, units);
			}
			
			tempMap = services.calcPercentile( "Pressure", dma, period, startTime.get(j), endTime.get(j));

			valPress = tempMap.get("mid");
			
			if(valPress != null){
				inner.put("Average Pressure Value ", valPress);
			}
			map.put("campaign"+ (j+1), inner);
		}

		return mapToData(map);
	}
	
	/*for (String dmat : dmaSet){
		
	Double averageVal;	
	if ("Real losses".equalsIgnoreCase(timeSerSel)){
		innermap=(services.getRealLoss(timeSerSel,dmat, period,  from, to,  units));
	}
	else if ("Inflow".equalsIgnoreCase(timeSerSel)){
		innermap=(services.getInflow(timeSerSel,dmat, period,  from, to,  units));
	}
	else {
		innermap=services.getTimeSeries(period, dmat, from, to, timeSerSel, units);
	}
	
//	Double value;
//	String key;
	inner=services.calcPercentile( "Pressure", dmat, period, from, to);

	averageVal =  (services.getAverage(innermap));
	
	minPress = services.getTimeSeries(period, dmat, from, to, "minPressure", units);
//	Set set = minPress.entrySet();
//	Iterator i = set.iterator();
//	
//	Map.Entry me = (Map.Entry)i.next();
//	String keyPress = (String) me.getKey();
//	Double minPressure = (Double) me.getValue();
	
	
	if (dmaSelected!=null ){
		inmapAvVal.put(dmat, averageVal);
		inner.putAll(minPress);
		maps.get(0).put("Average value",inmapAvVal);
		maps.get(1).put(dmat, inner);
	}
	}
	return mapsToData(maps);
	*/
	
	
	public DBData getLeakage(String timeSerSel, String dmaSelected, String period, String startTime, String endTime, String units){
		
		Map<String, Map<String, Double>> map=new TreeMap<String, Map<String, Double>>();
				
		Map<String, Double> innermap = new TreeMap<String, Double>();
						
		innermap=services.getTimeSeries(period, dmaSelected, startTime, endTime, timeSerSel, units);

		map.put(dmaSelected,innermap);
		
		return mapToData(map);
		
	}
	
	public DBData getPercentile (String timeSerSel, String dmaSelected, String period, String startTime, String endTime){
	
		Map<String, Map<String, Double>> mapa=new TreeMap<String, Map<String, Double>>();
		
		Map<String, Double> zaPoslat = new LinkedHashMap<String, Double>();
		zaPoslat = services.calcPercentile(timeSerSel, dmaSelected, period, startTime, endTime);

	   
	    if (dmaSelected!=null)
	    	mapa.put(dmaSelected, zaPoslat);
	    
		return mapToData(mapa);
	}

	
	/**
	 * @param maps
	 * @return
	 */
	private DBData[] mapsToData(List<Map<String, Map<String, Double>>> maps) {
		ArrayList<DBData> db = new ArrayList<DBData>();
		for (Map<String, Map<String, Double>> map : maps){
			db.add(mapToData(map));
		}
		return db.toArray(new DBData[db.size()]);
	}

	/**
	 * Update the two maps each with the new inner map for the specific key
	 * @param maps
	 * @param key
	 * @param innermaps
	 * @return a list of two nested maps
	 */
	private List<Map<String, Map<String, Double>>> updateMaps(List<Map<String, Map<String, Double>>> maps, String key, List<Map<String, Double>> innermaps) {
		maps.get(0).put(key,innermaps.get(0));
		maps.get(1).put(key,innermaps.get(1));
		return maps;		
	}

	/**
	 * @param map the double map containing the names, categories, values
	 * @return the DBData object with the respective names, categories, values
	 */
	private DBData mapToData(Map<String, Map<String, Double>> map) {
		DBData data = new DBData(map.entrySet().size());
		Iterator outerIt = map.entrySet().iterator();
		int outercnt=0;
		while (outerIt.hasNext()) {
			Map.Entry outerPairs = (Map.Entry)outerIt.next();
			Iterator innerIt = ((Map<String, Double>) outerPairs.getValue()).entrySet().iterator();
			int innercnt=0;
			String name = outerPairs.getKey().toString();
			data.names.add(name);
			data.values[outercnt] = new Double[((Map<String, Double>) outerPairs.getValue()).entrySet().size()];
			while (innerIt.hasNext()) {
				Map.Entry innerPairs = (Map.Entry)innerIt.next();
				data.values[outercnt][innercnt++] = (Double) innerPairs.getValue();
				data.categories.add(innerPairs.getKey().toString());
			}
			outercnt++;
		}
		data.unitsFromServer = new ArrayList<String>(services.getUnitsFromServer());
		services.getUnitsFromServer().clear();
		
		return data;
	}


	/**
	 * @return list of nested maps
	 */
	private List<Map<String, Map<String, Double>>> initMaps() {
		List<Map<String, Map<String, Double>>> maps = new ArrayList<Map<String,Map<String,Double>>>();
		for (int i=0;i<2;i++)
			maps.add( new TreeMap<String, Map<String,Double>>());
		return maps;
	}

	/**
	 * @param innermaps
	 * @return list of inner maps
	 */
	private List<Map<String, Double>> initInnerMaps(List<Map<String, Double>> innermaps) {
		innermaps.add(new TreeMap<String, Double>());
		innermaps.add(new TreeMap<String, Double>());
		return innermaps;
	}

	@Override
	public int getMeterCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getDMACount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getHouseholdTypeCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setupDBconnection() {
	}

	@Override
	public DBData getConsumerTypes() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
////	public DBData getOverallConsumption(String time_period) {
////		// TODO Auto-generated method stub
////		return null;
////	}

	@Override
	public DBData getConsumptionCategories(String dma, String resolution,String from, String to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DBData getConsumptionCategoriesPerDMA(String from, String to) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public DBData getInOutFlow(String dma, String resolution, String from,String to) {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	@Override
//	public DBData getInOutFlowPerDMA(String from, String to) {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
