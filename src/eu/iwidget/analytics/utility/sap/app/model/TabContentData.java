package eu.iwidget.analytics.utility.sap.app.model;

import java.util.ArrayList;

/**
 * container class for data of a tab
 * @author kristina_kirsten
 *
 */
public class TabContentData implements java.io.Serializable {
	
	public String title;
	public FilterData[] filters;
	public ChartData[] charts;
	public TableWarning[] tables;
	public final int id;
	
	private static int count = 0;
	
	public TabContentData() { this.id = count++; }
}
