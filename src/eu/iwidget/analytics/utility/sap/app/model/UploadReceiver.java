package eu.iwidget.analytics.utility.sap.app.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.vaadin.ui.Label;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Diogo Franco
 * This class implements Vaadin's Receiver and SucceededListener, which are fired when an upload
 * starts and finishes successfully, respectively
 */
public class UploadReceiver implements Receiver, SucceededListener {
	private static final long serialVersionUID = 1L;
	public static final String folderName = "uploads";
	public String fileUploaded = null;
	public File file;
	public VerticalLayout uploadLayout = new VerticalLayout();

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		if(this.uploadLayout != null) this.uploadLayout.removeAllComponents();
		this.uploadLayout.addComponent(new Label(event.getFilename())); 
		this.uploadLayout.addComponent(new Label("uploaded successfully"));
		fileUploaded = new String(folderName + File.separator + event.getFilename());	
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fos = null; //Stream to write to
		
		new File(folderName).mkdir(); //make folder "uploads" if it doesn't exist
		
		String fileForWriting = folderName + File.separator + filename;
		this.file = new File(fileForWriting);
		
		try {
			fos = new FileOutputStream(this.file);
		} catch (FileNotFoundException e) {
			Notification.show("Sorry, couldn't find " + fileUploaded);
			return null;
		}
		
		return fos;
	}

}
