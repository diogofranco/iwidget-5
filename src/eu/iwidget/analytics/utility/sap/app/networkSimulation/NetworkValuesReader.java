package eu.iwidget.analytics.utility.sap.app.networkSimulation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

import com.vaadin.ui.Notification;

import eu.iwidget.analytics.utility.sap.app.model.ChartData;
import eu.iwidget.analytics.utility.sap.app.model.SceneryFactory;
import eu.iwidget.analytics.utility.sap.app.model.Services;

public class NetworkValuesReader {

	public NetworkValueList[] averageNodeValues;
	public NetworkValueList[] averageLinkValues;

	public NetworkValuesReader(){
		this.averageNodeValues = new NetworkValueList[SceneryFactory.NR_SCENERIES];
		this.averageLinkValues = new NetworkValueList[SceneryFactory.NR_SCENERIES];
		for(int i = 0; i < SceneryFactory.NR_SCENERIES; i++){
			this.averageNodeValues[i] = new NetworkValueList();
			this.averageLinkValues[i] = new NetworkValueList();
		}	
	}

	public void readAll(){
		int i = 0;
		BufferedReader brNodes = null;		
		BufferedReader brLinks = null;
		for(String sceneryFile : SceneryFactory.sceneryFiles){
			String linksFile = sceneryFile + ".links.out";
			String nodesFile = sceneryFile + ".nodes.out";

			try{
				brNodes = new BufferedReader(new FileReader(nodesFile));
				brLinks = new BufferedReader(new FileReader(linksFile));
				String line;
				String currentTime = null;
				Double weight = 0D;
				Double averageValue = 0D;
				while ((line = brNodes.readLine()) != null) {
					//create NetworkNode for each line, putting it into allSceneryValues[i]
					String[] parts = line.split("\\t");
					if(!parts[0].isEmpty()){ // if line is relevant
						if(currentTime == null) currentTime = parts[1];
						if(currentTime.equals(parts[1])){
							Double demand;
							if((demand = Double.parseDouble(parts[6].replace(",","."))) > 0){ //considering only cases where demand is > 0
								averageValue = averageValue + 
										(Double.parseDouble(parts[4].replace(",",".")) * demand);
								weight = weight + demand;
							}
						}

						else{ //new currentTime
							averageValue = (weight == 0 ? 0 : averageValue / weight);
							NetworkNode node = new NetworkNode(currentTime, 
									averageValue);
							this.averageNodeValues[i].add(node);
							averageValue = 0D;
							weight = 0D;
							currentTime = parts[1];
						}
					}
				}
				/* adding last average value to the list and re-setting values */
				brNodes.close();
				averageValue = (weight == 0 ? 0 : averageValue / weight);
				NetworkNode node = new NetworkNode(currentTime, averageValue);
				this.averageNodeValues[i].add(node);
				averageValue = 0D;
				weight = 0D;
				currentTime = null;
				/* Debugging print
				Notification.show(this.averageNodeValues[0].size() +
						this.averageNodeValues[0].get(0).getTime() + " " + 
						((NetworkNode) this.averageNodeValues[0].get(0)).getPressure() + "<br/>" + 
						this.averageNodeValues[0].get(1).getTime() + " " +
						((NetworkNode) this.averageNodeValues[0].get(1)).getPressure());*/

				while ((line = brLinks.readLine()) != null) {
					//create NetworkLink for each line, putting it into allSceneryValues[i]
					String[] parts = line.split("\\t");
					if(!parts[0].isEmpty()){ // if line is relevant
						if(currentTime == null) currentTime = parts[1];
						if(currentTime.equals(parts[1])){
							Double diameter = Double.parseDouble(parts[3].replace(",","."));
							Double length = Double.parseDouble(parts[2].replace(",","."));
							Double volume = length * diameter * diameter;

							averageValue = averageValue + 
									(Double.parseDouble(parts[6].replace(",",".")) * volume);
							weight = weight + volume;	
						}

						else{ //new currentTime
							averageValue = (weight == 0 ? 0 : averageValue / weight);
							NetworkLink link = new NetworkLink(currentTime, 
									averageValue);
							this.averageLinkValues[i].add(link);
							averageValue = 0D;
							weight = 0D;
							currentTime = parts[1];
						}
					}
				}
				
				/* adding last average value to the list and re-setting values */
				brLinks.close();
				averageValue = (weight == 0 ? 0 : averageValue / weight);
				NetworkLink link = new NetworkLink(currentTime, 
						averageValue);
				this.averageLinkValues[i].add(link);
				averageValue = 0D;
				weight = 0D;
				currentTime = null;
				
				/* Debugging print
				 * 
				 * Notification.show(this.averageLinkValues[0].size() + 
						this.averageLinkValues[0].get(0).getTime() + " " + 
				((NetworkLink) this.averageLinkValues[0].get(0)).getVelocity() +
				this.averageLinkValues[0].get(1).getTime() + " " +
				((NetworkLink) this.averageLinkValues[0].get(1)).getVelocity());*/
			} catch(FileNotFoundException e){
				Notification.show(
						"Sorry, there was a problem with the EPANet tool, can't find the .out files!");
			} catch (IOException e) {
				Notification.show(
						"Sorry, there was a problem with the EPANet tool, can't open the .out files!");
			}
			i++;
		}
	}

	public void drawMinPressureChart(ChartData chart, Double hMin, String[] selectedFilter) {
		Double vals[][] = new Double
				[this.averageNodeValues.length][this.averageNodeValues[0].size()];
		String[] seriesNames = new String[this.averageNodeValues.length];
		Double fromField = Double.parseDouble(selectedFilter[2].replace("%",""));
		Double intervalField = Double.parseDouble(selectedFilter[4].replace("%",""));
		
		for(int i = 0; i < this.averageNodeValues.length;i++){
			int k = 0;
			for(NetworkValue value : this.averageNodeValues[i].oneSceneryValues){
				chart.categories.add(value.getTime());
				vals[i][k++] = Services.gradeMinPressure((NetworkNode) value, hMin);
			}
			
			Double serName = (Double) fromField + i * intervalField;
			seriesNames[i] = ((Integer)serName.intValue()).toString() + "% hypothesis";
		}
		
		chart.seriesNames = seriesNames;
		chart.values = vals;

		ArrayList<String> ufs = new ArrayList<String>();
		ufs.add("");
		chart.unitsFromServer = ufs;
		
	}
	
	public void drawMaxPressureChart(ChartData chart, Double hMin, Double hMax, String[] selectedFilter) {
		Double vals[][] = new Double
				[this.averageNodeValues.length][this.averageNodeValues[0].size()];
		String[] seriesNames = new String[this.averageNodeValues.length];
		Double fromField = Double.parseDouble(selectedFilter[2].replace("%",""));
		Double intervalField = Double.parseDouble(selectedFilter[4].replace("%",""));
		
		for(int i = 0; i<this.averageNodeValues.length;i++){
			int k = 0;
			for(NetworkValue value : this.averageNodeValues[i].oneSceneryValues){
				chart.categories.add(value.getTime());
				vals[i][k++] = Services.gradeMaxPressure((NetworkNode) value, hMin, hMax);
			}
			Double serName = (Double) fromField + i * intervalField;
			seriesNames[i] = ((Integer)serName.intValue()).toString() + "% hypothesis";
		}
		chart.seriesNames = seriesNames;
		chart.values = vals;

		ArrayList<String> ufs = new ArrayList<String>();
		ufs.add("");
		chart.unitsFromServer = ufs;	
	}
	
	public void drawMinVelocityChart(ChartData chart, Double vRef, String[] selectedFilter) {
		Double vals[][] = new Double
				[this.averageLinkValues.length][this.averageLinkValues[0].size()];
		String[] seriesNames = new String[this.averageLinkValues.length];
		Double fromField = Double.parseDouble(selectedFilter[2].replace("%",""));
		Double intervalField = Double.parseDouble(selectedFilter[4].replace("%",""));
		
		for(int i = 0; i<this.averageLinkValues.length;i++){
			int k = 0;
			for(NetworkValue value : this.averageLinkValues[i].oneSceneryValues){
				chart.categories.add(value.getTime());
				vals[i][k++] = Services.gradeMinVelocity((NetworkLink) value, vRef);
			}
			Double serName = (Double) fromField + i * intervalField;
			seriesNames[i] = ((Integer)serName.intValue()).toString() + "% hypothesis";
		}

		chart.seriesNames = seriesNames;
		chart.values = vals;

		ArrayList<String> ufs = new ArrayList<String>();
		ufs.add("");
		chart.unitsFromServer = ufs;
		
	}
}
