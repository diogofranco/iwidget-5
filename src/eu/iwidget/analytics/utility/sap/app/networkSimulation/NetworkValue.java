package eu.iwidget.analytics.utility.sap.app.networkSimulation;

/**
 * @author diogofranco
 * 
 * This class is inherited by NetworkLink and NetworkNode,
 * which hold the values simulated from the EPANet tool. 
 */
public abstract class NetworkValue {

	private String time;
	
	public NetworkValue(String time){
		this.time = time;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
}
