package eu.iwidget.analytics.utility.sap.app.networkSimulation;

import java.util.ArrayList;


/**
 * @author diogofranco
 * 
 * This class just holds a list of NetworkValues.
 * It was needed as a small hack to create an array of 
 * lists for the different sceneries, in NetworkValuesReader.
 */
public class NetworkValueList {
	
	public ArrayList<NetworkValue> oneSceneryValues;
	
	public NetworkValueList(){
		this.oneSceneryValues = new ArrayList<NetworkValue>();
	}
	
	public void add(NetworkValue value){
		this.oneSceneryValues.add(value);
	}
	
	public NetworkValue get(int index){
		return this.oneSceneryValues.get(index);
	}
	
	public int size(){
		return this.oneSceneryValues.size();
	}
}
