package eu.iwidget.analytics.utility.sap.app.networkSimulation;


public class NetworkNode extends NetworkValue{

	private Double pressure;
	
	public NetworkNode(String time, Double pressure){
		super(time);
		this.pressure = pressure;	
	}
	
	public Double getPressure() {
		return pressure;
	}
	
	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}
}
