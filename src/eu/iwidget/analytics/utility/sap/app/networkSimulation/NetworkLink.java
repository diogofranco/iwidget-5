package eu.iwidget.analytics.utility.sap.app.networkSimulation;


public class NetworkLink extends NetworkValue{

	private Double velocity;
	
	public NetworkLink(String time,	Double velocity) {
		super(time);
		this.velocity = velocity;
	}

	public Double getVelocity() {
		return velocity;
	}

	public void setVelocity(Double velocity) {
		this.velocity = velocity;
	}
}
