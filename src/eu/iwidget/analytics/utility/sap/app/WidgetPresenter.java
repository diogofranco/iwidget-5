package eu.iwidget.analytics.utility.sap.app;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import com.vaadin.ui.Notification;
import eu.iwidget.analytics.utility.sap.app.model.ChartData;
import eu.iwidget.analytics.utility.sap.app.model.FilterData;
import eu.iwidget.analytics.utility.sap.app.model.TableWarning;
import eu.iwidget.analytics.utility.sap.app.model.TabContentData;
import eu.iwidget.analytics.utility.sap.app.model.WidgetModel;
import eu.iwidget.analytics.utility.sap.app.model.WidgetModel.DBData;
import eu.iwidget.analytics.utility.sap.app.view.WidgetView;
import eu.iwidget.analytics.utility.sap.app.view.WidgetViewImpl;

public class WidgetPresenter implements WidgetView.TabViewListener{

	public static final String DeviceTypeInflow = "Inflow";
	public static final String DeviceTypeOutflow = "Outflow";
	public static final String DeviceTypeConsumption = "Output";
	
	private WidgetModel model=null;
	private WidgetViewImpl view;
	private TabContentData[] tabContent;
	
	/**
	 * constructor creates connection between model and presenter AND view and presenter
	 * @param model the model which operates with the database and implements the WidgetModel
	 * @param view the view for showing the data
	 */	
	public WidgetPresenter(WidgetModel model, WidgetViewImpl view) {
		this.model = model;
		this.view = view;

		TabContentData[] t = {createPressureControlTab(),
				createDMAanalysisTab(), createCampaignsTab(), 
				createComplaintsTab(), createWarningsTab(),
				createSimulationsTab()};
		this.tabContent = t;
		this.view.setupLayout(tabContent);

		//this presenter implements tabViewListener
		this.view.addListener(this);
	}
	
	
	
	private TabContentData createComplaintsTab() {
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC05.2 - Complaints";
		
		//filter data
		int numFilters = 2;
		FilterData[] filters = new FilterData[numFilters];
		tab.filters = filters;
		tab.filters[0] = clientIdFilter();
		tab.filters[1] = complaintsDatePicker();
		
		int amountCharts = 0;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
		
		return tab;
	}


	private TabContentData createSimulationsTab() {
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC07.2 - Network Expansions";
		
		//filter data
		int numFilters = 9;
		FilterData[] filters = new FilterData[numFilters];
		tab.filters = filters;
		
		tab.filters[0] = uploadFilter();
		tab.filters[1] = uploadPatternFilter();
		tab.filters[2] = demandCheckboxFilter();
		tab.filters[3] = demandFilter("from");
		tab.filters[4] = demandFilter("to");
		tab.filters[5] = demandIntervalFilter();
		tab.filters[6] = referenceValuesFilter("minPressure");
		tab.filters[7] = referenceValuesFilter("maxPressure");
		tab.filters[8] = referenceValuesFilter("velocity");

		
		int amountCharts = 3;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
		
		//chart0
		String domId =  "chart0";
		String ldomId = "legend0";
		String chartOriginalTitle = "Minimum Pressure";
		String chartTitle = "Minimum Pressure";
		String chartDescription = "";
		String originalYAxisTitle = "Performance Index (-)";
		String yAxisTitle = "Performance Index (-)";
		String xAxisFormatter = "function() { this.value }";
		String xAxisTitle = "Time";
		String yAxisUnit = "";
		String tooltip = "";

		tab.charts[0] = setupChart(domId, ldomId, chartTitle, chartOriginalTitle,
				chartDescription,xAxisTitle, xAxisFormatter, yAxisTitle,
				 originalYAxisTitle, yAxisUnit,true, false, ChartType.LINECHART, tooltip);
		
		//chart1
		String domId1 =  "chart1";
		String ldomId1 = "legend1";
		String chartOriginalTitle1 = "Maximum Pressure";
		String chartTitle1 = "Maximum Pressure";
		String chartDescription1 = "";
		String originalYAxisTitle1 = "Performance Index (-)";
		String yAxisTitle1 = "Performance Index (-)";
		String xAxisFormatter1 = "function() { this.value }";
		String xAxisTitle1 = "Time";
		String yAxisUnit1 = "";
		String tooltip1 = "";

		tab.charts[1] = setupChart(domId1, ldomId1, chartTitle1, chartOriginalTitle1,
				chartDescription1,xAxisTitle1, xAxisFormatter1, yAxisTitle1,
				 originalYAxisTitle1, yAxisUnit1,true, false, ChartType.LINECHART, tooltip1);
		
		//chart2
		String domId2 =  "chart2";
		String ldomId2 = "legend2";
		String chartOriginalTitle2 = "Minimum Velocity";
		String chartTitle2 = "Minimum Velocity";
		String chartDescription2 = "";
		String originalYAxisTitle2 = "Performance Index (-)";
		String yAxisTitle2 = "Performance Index (-)";
		String xAxisFormatter2 = "function() { this.value }";
		String xAxisTitle2 = "Time";
		String yAxisUnit2 = "";
		String tooltip2 = "";

		tab.charts[2] = setupChart(domId2, ldomId2, chartTitle2, chartOriginalTitle2,
				chartDescription2,xAxisTitle2, xAxisFormatter2, yAxisTitle2,
				 originalYAxisTitle2, yAxisUnit2,true, false, ChartType.LINECHART, tooltip2);
		return tab;
	}

	private TabContentData createWarningsTab() {
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC05.3 - Warnings";
		
		//filter data
		int numFilters = 3;
		FilterData[] filters = new FilterData[numFilters];
		tab.filters = filters;
		
		tab.filters[0] = networkSelectFilter("DMA 1", "DMA 2", "DMA 3", " ");
		tab.filters[1] = toleranceFilter();
		tab.filters[2] = timeIntervalFilter();
		
		int amountCharts = 0;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
		
		int amountTableWarnings = 0;
		TableWarning[] tw = new TableWarning[amountTableWarnings];
		tab.tables = tw;
		return tab;
	}



	private TabContentData createPressureControlTab(){
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC04.3 - Pressure Overview";
		
		
		//create filter data
		int amountFilters = 4;
		FilterData[] filters = new FilterData[amountFilters];
		tab.filters = filters;
			
		//filter0	
		tab.filters[0] = timeSeriesSelectFilter();
		//filter1
		tab.filters[1] = networkSelectFilter("DMA 1", "DMA 2", "DMA 3", "all DMAs");
		//filter2	
		tab.filters[2] = timePeriodFilter();				
		//filter3	
		tab.filters[3] = timeIntervalFilter();	
		//filter3
		//tab.filters[3] = meterSelectFilter();
		//filter4
		//tab.filters[4] = consCategorySelectFilter();
				
		//create chart data
		int amountCharts = 2;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
								
		//chart0
		String domId =  "chart0";
		String ldomId = "legend0";
		String chartOriginalTitle = "Time Series";
		String chartTitle = "Time Series";
		String chartDescription = "";
		String originalYAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String yAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String xAxisFormatter = "function() { this.value }";
		String xAxisTitle = "date";
		String yAxisUnit = "";
		String tooltip = "function() {"
				+ "var date=new Date(this.x);"
				+ "var dateStr=date.toDateString(); "
				+ "if (isNaN(date.getTime())) "
				+ "		dateStr=this.x;"
				+ "return '<b>' + this.series.name + '</b><br/>' + dateStr + ': '+ this.y.toFixed(2) +'m�'; " + "}";

		tab.charts[0] = setupChart(domId, ldomId, chartTitle, chartOriginalTitle,
				chartDescription,xAxisTitle, xAxisFormatter, yAxisTitle,
				 originalYAxisTitle, yAxisUnit,true, false, ChartType.LINECHART, tooltip);
		
		
		//chart1
		String domId1 =  "chart1";
		String ldomId1 = "legend1";
		String chartOriginalTitle1 = "Downstream Pressure";
		String chartTitle1 = "Downstream Pressure";
		String chartDescription1 = "";
		String originalYAxisTitle1 = "Pressure (kPa)";
		String yAxisTitle1 = "Pressure (kPa)";
		String xAxisFormatter1 = "function() { this.value }";
		String xAxisTitle1 = "date";
		String yAxisUnit1 = "";
		String tooltip1 = "function() {"
				+ "var date=new Date(this.x);"
				+ "var dateStr=date.toDateString(); "
				+ "if (isNaN(date.getTime())) "
				+ "		dateStr=this.x;"
				+ "return '<b>' + this.series.name + '</b><br/>' + dateStr + ': '+ this.y.toFixed(2) +'m�'; " + "}";

		tab.charts[1] = setupChart(domId1, ldomId1, chartTitle1, chartOriginalTitle1,
				chartDescription1,xAxisTitle1, xAxisFormatter1, yAxisTitle1,
				originalYAxisTitle1,yAxisUnit1,true, false, ChartType.LINECHART, tooltip1);
		return tab;
	}
	
	private TabContentData createDMAanalysisTab(){
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC04.3 - Pressure Statistics";
		
		//create filter data
		int amountFilters = 4;
		FilterData[] filters = new FilterData[amountFilters];
		tab.filters = filters;
		
		//filter0	
		tab.filters[0] = timeSeriesSelectFilter();
		//filter1
		tab.filters[1] = networkSelectFilter("DMA 1", "DMA 2", "DMA 3", "all DMAs");
		//filter2	
//		tab.filters[2] = userInputField("Minimum Pressure Value (bar)",  "0");
		//filter3	
		tab.filters[2] = timePeriodFilter();
		//filter4	
		tab.filters[3] = timeIntervalFilter();	
		//filter5
		//tab.filters[5] = meterSelectFilter();
		//filter6
		//tab.filters[6] = consCategorySelectFilter();
		
		//create chart data
		int amountCharts = 2;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
						
				
		//chart0
		String domId =  "chart0";
		String ldomId = "legend0";
		String chartOriginalTitle = "Time Series Average Value";
		String chartTitle = " Time Series Average Value ";
		String chartDescription = " ";
		String originalYAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String yAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String xAxisFormatter = "function() { this.value }";
		String xAxisTitle = " ";
		String yAxisUnit = "";
		String tooltip = "function() {" + " return '<b>' + this.series.name + '</b><br/>' +  this.x + ': '+ this.y.toFixed(2) +'m�'; " + "}";

		tab.charts[0] = setupChart(domId, ldomId, chartTitle, chartOriginalTitle, 
				chartDescription,xAxisTitle, xAxisFormatter, yAxisTitle,
				originalYAxisTitle,yAxisUnit,true, false, ChartType.BARCHART, tooltip);
		
		//chart1
		String domId1 =  "chart1";
		String ldomId1 = "legend1";
		String chartOriginalTitle1 ="Pressure Variation";
		String chartTitle1 = "Pressure Variation";
		String chartDescription1 = "";
		String originalYAxisTitle1 = "Pressure (kPa)";
		String yAxisTitle1 = "Pressure (kPa)";
		String xAxisFormatter1 = "function() { this.value }";
		String xAxisTitle1 = "";
		String yAxisUnit1 = "";
		String tooltip1 = " ";

		tab.charts[1] = setupChart(domId1, ldomId1, chartTitle1, chartOriginalTitle1, 
				chartDescription1,xAxisTitle1,xAxisFormatter1,yAxisTitle1,
				 originalYAxisTitle1,yAxisUnit1,true, false, ChartType.CANDLESTICKCHART, tooltip1);
		return tab;
	}

	private TabContentData createCampaignsTab(){
		TabContentData tab = new TabContentData();
		tab.title = "WU-UC04.3 - Pressure Campaigns";
				
		
		//create filter data
		int amountFilters = 9; //jpcoelho: changed from 13 to 7 (only 2 campaigns as default)	
		FilterData[] filters = new FilterData[amountFilters];
		tab.filters = filters;
				
		//filter0	
		tab.filters[0] = timeSeriesSelectFilter();
		//filter1
		tab.filters[1] = networkSelectFilter("DMA 1", "DMA 2", "DMA 3", " ");
		//filter2	
		tab.filters[2] = timePeriodFilter();
		
		//filter3	
		tab.filters[3] = userInputField("Campaign 1 Start time", "yyyy-mm-dd HH:mm:ss");
		//filter4	
		tab.filters[4] = userInputField("Campaign 1 End time", "yyyy-mm-dd HH:mm:ss");
		//filter5	
//		tab.filters[5] = userInputField("Campaign 1 Pressure Value", "0");
		//filter5	
		tab.filters[5] = userInputField("Campaign 2  Start time", "yyyy-mm-dd HH:mm:ss");
		//filter6	
		tab.filters[6] = userInputField("Campaign 2  End time", "yyyy-mm-dd HH:mm:ss");
		//filter7	
		tab.filters[7] = userInputField("Campaign 3  Start time", "yyyy-mm-dd HH:mm:ss");
		//filter8	
		tab.filters[8] = userInputField("Campaign 3  End time", "yyyy-mm-dd HH:mm:ss");
//		//filter9	
//		tab.filters[9] = userInputField("Campaign 4  Start time", "yyyy-mm-dd HH:mm:ss");
//		//filter10	
//		tab.filters[10] = userInputField("Campaign 4  End time", "yyyy-mm-dd HH:mm:ss");
//		//filter11	
//		tab.filters[11] = userInputField("Campaign 5  Start time", "yyyy-mm-dd HH:mm:ss");
//		//filter12	
//		tab.filters[12] = userInputField("Campaign 5  End time", "yyyy-mm-dd HH:mm:ss");
		//filter8	
//		tab.filters[8] = userInputField("Campaign 2  Pressure Value",  "0");
//		//filter3	
//		tab.filters[3] = timePeriodFilter();	
//		//filter4	
//		tab.filters[4] = timeIntervalFilter();		

		//create chart data
		int amountCharts = 1;
		ChartData[] charts = new ChartData[amountCharts];
		tab.charts = charts;
							
		//chart0
		String domId =  "chart0";
		String ldomId = "legend0";
		String chartOriginalTitle = " ";
		String chartTitle = " ";
		String chartDescription = "Comparison of Time Series with Pressure";
		String originalYAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String yAxisTitle = "m" + new String(new char[] {179}) + "/h";
		String xAxisFormatter = "function() { this.value }";
		String xAxisTitle = "Campaign";
		String yAxisUnit = "";
		String tooltip = "  ";
		
		tab.charts[0] = setupChart(domId, ldomId, chartTitle, chartOriginalTitle,
				chartDescription,xAxisTitle,xAxisFormatter, yAxisTitle, originalYAxisTitle,
				yAxisUnit,true, false, ChartType.CANDLESTICKCHART, tooltip);
	
		
//		//chart1
//		String domId1 =  "chart1";
//		String ldomId1 = "legend1";
//		String chartTitle1 = " ";
//		String chartDescription1 = " ";
//		String yAxisTitle1 = " (Pa/10000)(cms) ";
//		String xAxisFormatter1 = "function() { this.value }";
//		String xAxisTitle1 = "Campaign";
//		String yAxisUnit1 = "";
//		String tooltip1 = "  ";
//				
//		tab.charts[1] = setupChart(domId1, ldomId1, chartTitle1, chartDescription1, xAxisTitle1, xAxisFormatter1, yAxisTitle1, yAxisUnit1, true, false, ChartType.CANDLESTICKCHART, tooltip1);
		return tab;
	}
	
/**
 * creates a new chart data object
 * @param title the title of the chart
 * @param description a further description what the chart is showing
 * @param xAxisTitle the title of the x-axis
 * @param yAxisTitle the title of the y-axis
 * @param yAxisUnit the unit for the y-axis
 * @param legend boolean value for showing legend or not
 * @param type the chart type
 * @return chart data object 
 */
private ChartData setupChart(String domId, String ldomId, String title, String originalTitle,
		String description, String xAxisTitle, String xAxisFormatter, String yAxisTitle,
		String originalYAxisTitle, String yAxisUnit, boolean legend, boolean dataTable,
		ChartType type, String tooltip) {
	
	ChartData chart = new ChartData();

	chart.domId = domId;
	chart.ldomId = ldomId;
	chart.originalTitle = originalTitle;
	chart.title = title;
	chart.description = description;
	chart.legend = legend;
	chart.xAxisTitle = xAxisTitle;
	chart.yAxisTitle = yAxisTitle;
	chart.originalYAxisTitle = originalYAxisTitle;
	chart.yAxisUnit = yAxisUnit;
	chart.values = new Double[0][0];
	chart.categories = new LinkedHashSet<String>();
	chart.seriesNames = new String[0];
	chart.type = type;

	return chart;
}

/**
 * creates a new filter data object
 * @param title the title for the filter
 * @param type type of the filter, e.g. dropdown, checkbox..
 * @param category category, e.g. temporal_resolution, time_interval..
 * @param values the values to be used in the filter
 * @param defaultValue the default value to be selected initially 
 * @param HTMLid string for identifying filter in tab
 * @return filter data object
 */
private FilterData initializeFilter(String title, FilterType type, FilterCategory category,
		Map<Integer, String> values, Object defaultValue, String HTMLid) {
	FilterData filter = new FilterData();
	
	filter.title = title;
	filter.type = type;
	filter.category = category;
	filter.values = values;
	filter.defaultValue = defaultValue;
	filter.id = HTMLid;
	
	return filter;
	}


/**
 * @param selectedFilter filter number
 * @param secTabChoice secondary tab choice (meterOverview, DMAinflowCons, consCat)
 * It is executed if the button at the filter section is clicked
 */

public void buttonClick(String[] selectedFilter, int tabID) {
	DBData[] data=null;
	boolean filtersFilled = checkIfFiltersFilled(selectedFilter);
	if(!filtersFilled){
		return;
	}
	
	if (this.tabContent[0].id == tabID){
		drawCharts(this.tabContent[0], this.model.firstTab(selectedFilter[0],selectedFilter[1],
				selectedFilter[2], selectedFilter[3], selectedFilter[4]), filtersFilled, selectedFilter);
	}
	if (this.tabContent[1].id == tabID){
		drawCharts(this.tabContent[1], this.model.secondTab(selectedFilter[0],selectedFilter[1],
				selectedFilter[2], selectedFilter[3], selectedFilter[4]), filtersFilled, selectedFilter);
	}
	if (this.tabContent[2].id == tabID){
		//jpcoelho: edited to have only 2 campaigns
		data=new DBData[] {this.model.thirdTab(selectedFilter[0],selectedFilter[1], selectedFilter[2], selectedFilter[3], selectedFilter[4], selectedFilter[5],selectedFilter[6], selectedFilter[7], selectedFilter[8])};
//		data=new DBData[] {this.model.thirdTab(selectedFilter[0],selectedFilter[1], selectedFilter[2], selectedFilter[3], selectedFilter[4], selectedFilter[5],
//				selectedFilter[6], selectedFilter[7], selectedFilter[8], selectedFilter[9], selectedFilter[10], selectedFilter[11], selectedFilter[12])};
		drawCharts (this.tabContent[2], data, filtersFilled, selectedFilter );
//		drawCharts(this.tabContent[2], this.model.thirdTab(selectedFilter[0],selectedFilter[1], selectedFilter[2], selectedFilter[3], selectedFilter[4], selectedFilter[5],
//				selectedFilter[6]));
	}
}

/**
 * sets the new chart data after updating
 * @param tab the tab data
 * @param data the data received from DB in model
 */
private void drawCharts(TabContentData tab, DBData[] data, boolean filtersFilled, String[] selectedFilter){
	if (data!=null){
		for(int i=0; i<data.length; i++){
			if(data[i] != null){
				tab.charts[i].categories = data[i].categories;
				tab.charts[i].seriesNames = data[i].names.toArray(new String[0]);
				
				refreshTitles(tab, selectedFilter, filtersFilled, i);
				
				tab.charts[i].values = data[i].values;
				
				tab.charts[i].unitsFromServer = data[0].unitsFromServer;
			}
		}

		for(ChartData c : tab.charts) c.setValues();
	}
	
	if(filtersFilled){
		for(ChartData chart : tab.charts){
			for(Double[] v : chart.values){
				if(v.length == 0){
					Notification.show("Some data is not available in the selected time period!");
					break;
				}
				for(Double val : v){
					if(val.isNaN()){
						Notification.show("Some data is not available in the selected time period!");
						break;
					}
				}
			}
		}
	}
}



/**
 * This method puts the right titles upon rendering a chart.
 * 
 * @param tab this tab data
 * @param selectedFilter the strings with the user input on the filters
 * @param filtersFilled true if all filters were filled by the user, false otherwise
 * @param i an integer which refers to which chart is this on a given tab
 * @return void
 */
private void refreshTitles(TabContentData tab, String[] selectedFilter, boolean filtersFilled, int i){

	if(filtersFilled){
		if (tab.id == 2){//campaigns tab doesn't have a title
			tab.charts[i].title = selectedFilter[0] + " and pressure statistics for each campaign in " + selectedFilter[1];
			tab.charts[i].yAxisTitle = selectedFilter[0] + " (" +
					tab.charts[i].originalYAxisTitle + ")";
		}
		else if(i == 1){ //second charts doesn't have time series in the title
			tab.charts[i].title = tab.charts[i].originalTitle + 
					" for " + selectedFilter[1];
		}
		else{
			tab.charts[i].title = selectedFilter[0] + " " + tab.charts[i].originalTitle + 
					" for " + selectedFilter[1];
			tab.charts[i].yAxisTitle = selectedFilter[0] + " (" +
					tab.charts[i].originalYAxisTitle + ")";
		}
		
		if(selectedFilter[2].equals("Daily") && (tab.title.contains("Pressure Overview") || 
				tab.title.contains("Pressure Statistics") || tab.title.contains("Pressure Campaigns"))){
			String original = tab.charts[i].yAxisTitle;
			String newTitle = null;
			if(original.substring(original.length() - 2, original.length()).equals("h)")){
				newTitle = new String(original.substring(0, original.length() - 2) + "day)");
			} else {
				newTitle = original;
			}
			tab.charts[i].yAxisTitle = new String(newTitle);
		}
	}
	else{
		tab.charts[i].title = tab.charts[i].originalTitle;
		tab.charts[i].yAxisTitle = tab.charts[i].originalYAxisTitle;
	}
}

/**
 * @param selectedFilter Array with the selected filters
 * @return true if no array is null, false otherwise.
 */
private boolean checkIfFiltersFilled(String[] selectedFilter){
	for(String s : selectedFilter){
		if(s == null) return false;
	}
	return true;
}

private FilterData timeSeriesSelectFilter() {
	String title2 = "Time Series";
	FilterType type2 = FilterType.DROPDOWN;
	FilterCategory category2 = FilterCategory.TIME_SERIES;
	Map<Integer, String> values2 = new HashMap<Integer, String>();
	values2.put(1, "Inflow");
	values2.put(2, "Billed Metered Consumption");
	values2.put(3, "Leakage"); 
	values2.put(4, "Pipe Bursts");
	values2.put(5, "Real Losses");
	return initializeFilter(title2, type2, category2, values2, values2.get(0), "timeSeries");
}

private FilterData timePeriodFilter() {
	String title3 = "Temporal Resolution"; // jpcoelho - changed "time period" to "temporal resolution" per Dalia's instructions
	FilterType type3 = FilterType.DROPDOWN;
	FilterCategory category3 = FilterCategory.TIME_PERIOD;
	Map<Integer, String> values3 = new HashMap<Integer, String>();
	values3.put(1, "Hourly");
	values3.put(2, "Daily");
	return initializeFilter(title3, type3, category3, values3, values3.get(0), "timePeriod");
}

/* Used to initialize a filter which lets us choose "last 7 days", "last 14 days" etc. 
private FilterData timeToTodayFilter() {
	String title = "Time Interval";
	FilterType type = FilterType.DROPDOWN;
	FilterCategory category = FilterCategory.TIME_TO_TODAY;
	Map<Integer, String> values = new HashMap<Integer, String>();
	values.put(1, "Last 7 days");
	values.put(2, "Last 14 days");
	values.put(3, "Last 30 days");
	return initializeFilter(title, type, category, values, values.get(0), "timeToTodayInput");
}*/

private FilterData timeIntervalFilter() {
	String title0 = "Time Interval";
	FilterType type0 = FilterType.DATEPICKER;
	FilterCategory category0 = FilterCategory.TIME_INTERVAL;

	Calendar curData = Calendar.getInstance();
	curData.set(Calendar.HOUR_OF_DAY, 0);
	curData.set(Calendar.MINUTE, 0);
	curData.set(Calendar.SECOND, 0);
	
	//prototype data, last available date: 2010-05-03 04:30:00
	//prototype data compressed: 2009-12-31 
	curData.set(2014, 9, 8);
	Calendar defaultValue0 = curData;
	return initializeFilter(title0, type0, category0, null, defaultValue0, "TimeInterval");
}

private FilterData networkSelectFilter(String one, String two, String three, String four) {
	String title1 = "Network Sector";
	FilterType type1 = FilterType.DROPDOWN;
	FilterCategory category1 = FilterCategory.NETWORK_SECTOR;
	Map<Integer, String> values1 = new HashMap<Integer, String>();
	values1.put(1, one);
	values1.put(2, two);
	values1.put(3, three);
	values1.put(4, four);
	return initializeFilter(title1, type1, category1, values1, values1.get(0), "DMAFilter");
}

//private FilterData pressureValueFilter(String title) {
//	String title4 = title;
//	FilterType type4 = FilterType.TEXTFIELD;
//	FilterCategory category4= FilterCategory.PRESSURE_VALUE;
//
//	return initializeFilter(title4, type4, category4, null, null, title);
//}

private FilterData userInputField(String title, String defaultValue) {
	FilterType type = FilterType.CAMPAIGNS_DATE_PICKER;
	FilterCategory category= FilterCategory.PRESSURE_VALUE;
	//Double defaultValue = 0.0;
	return initializeFilter(title, type, category, null, defaultValue, "UserValueInput");
}

private FilterData toleranceFilter() {
	String title = "Tolerance";
	FilterType type = FilterType.DROPDOWN;
	FilterCategory category = FilterCategory.TOLERANCE;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	values.put(1, "1E-7");
	values.put(2, "1E-6");
	values.put(3, "1E-5");
	values.put(4, "1E-4");
	values.put(5, "1E-3");
	
	return initializeFilter(title, type, category, values, values.get(0), "ToleranceInput");
}



private FilterData demandFilter(String s) {
	String title = null;
	if(s.equals("from")){
		title = "Change Demand";
	}
	else{
		title = "To";
	}
	
	FilterType type = FilterType.DROPDOWN;
	FilterCategory category = FilterCategory.DEMAND;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	values.put(1, "-20%");
	values.put(2, "-10%");
	values.put(3, "-5%");
	values.put(4, "0%");
	values.put(5, "+5%");
	values.put(6, "+10%");
	values.put(7, "+20%");
	
	String defaultValue;
	if(s.equals("from")){
		defaultValue = 	values.get(4);
	}
	else{
		defaultValue = values.get(6);
	}
	
	return initializeFilter(title, type, category, values, defaultValue, "DemandInputs");
}

private FilterData demandCheckboxFilter(){
	String title = "Analyze range of demands";
	FilterType type = FilterType.DEMAND_CHECKBOX;
	FilterCategory category = FilterCategory.DEMAND;
	
	return initializeFilter(title, type, category, null, false, "demandCheckboxInput");
}

private FilterData uploadFilter() {
	String title = "Browse input file (EPANET .inp file)";
	FilterType type = FilterType.UPLOAD;
	FilterCategory category = FilterCategory.UPLOAD;
	
	return initializeFilter(title, type, category, null, null, "UploadInput");
}

private FilterData uploadPatternFilter(){
	String title = "Update consumption pattern (EPANET .pat file) (opt)";
	FilterType type = FilterType.UPLOAD_PATTERN;
	FilterCategory category = FilterCategory.UPLOAD;
	
	return initializeFilter(title, type, category, null, null, "UploadPatternInput");
}

private FilterData demandIntervalFilter() {
	String title = "Intervals";
	FilterType type = FilterType.DROPDOWN;
	FilterCategory category = FilterCategory.DEMAND;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	values.put(1, "1%");
	values.put(2, "2%");
	values.put(3, "5%");
	values.put(4, "10%");	
	
	return initializeFilter(title, type, category, values, values.get(3), "demandIntervalInput");
}

private FilterData referenceValuesFilter(String s) {
	String title = null;
	if(s.equals("minPressure")){
		title = "Minimum Pressure (m)";
	}
	else if(s.equals("maxPressure")){
		title = "Maximum Pressure (m)";
	}
	else{
		title = "Velocity (m/s)";
	}
	FilterType type = FilterType.SLIDER;
	FilterCategory category = FilterCategory.DEMAND;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	return initializeFilter(title, type, category, values, 40D, "reverenceValuesInput");
}

private FilterData complaintsDatePicker() {
	String title = "Date";
	FilterType type = FilterType.COMPLAINTS_DATE_PICKER;
	FilterCategory category = FilterCategory.COMPLAINTS;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	return initializeFilter(title, type, category, values, "", "complaintsDateInput");
}



private FilterData clientIdFilter() {
	String title = "Client ID";
	FilterType type = FilterType.INPUT_FIELD;
	FilterCategory category = FilterCategory.COMPLAINTS;
	
	Map<Integer, String> values = new HashMap<Integer, String>();
	return initializeFilter(title, type, category, values, "", "clientIdInput");
}

/* private TabContentData createEmptyTab(TabContentData tab) {
	//create filter data
	int amountFilters = 0;
	FilterData[] filters = new FilterData[amountFilters];
	tab.filters = filters;
	
	//create chart data
	int amountCharts = 0;
	ChartData[] charts = new ChartData[amountCharts];
	tab.charts = charts;

	return tab;
	} */
}		
	
