package eu.iwidget.analytics.utility.sap.app.components.chart;

import java.util.ArrayList;

import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractJavaScriptComponent;

@SuppressWarnings("serial")
@JavaScript({ "chart.js", "chart_connector.js" })
public class Chart extends AbstractJavaScriptComponent {

    public Chart(String domId) {
        getState().domId = domId;
    }

    @Override
    protected ChartState getState() {
        return (ChartState) super.getState();
    }
	public void setDomId(String domId) {
	    getState().domId = domId;
	}

	public void setNames(String[] names) {
        getState().names = names;
    }
    
    public void setCat(String[] cat) {
        getState().cat = cat;
    }
    
    public void setChartType(String ctype) {
        getState().ctype = ctype;
    }

	public void setVal(Double[][] val) {
        getState().val = val;
	}
	
	public void setTitle(String title) {
		getState().title = title;
	}

	public void setxAxisTitle(String xAxisTitle) {
		getState().xAxisTitle = xAxisTitle;
	}

	public void setyAxisTitle(String yAxisTitle) {
		getState().yAxisTitle = yAxisTitle;
	}

	public void setLdomId(String ldomId) {
		getState().ldomId = ldomId;
	}
	
	public void setUnitsFromServer(ArrayList<String> unitsFromServer){
		getState().unitsFromServer = unitsFromServer;
	}
}
