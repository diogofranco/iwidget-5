package eu.iwidget.analytics.utility.sap.app.components.chart;

import java.util.ArrayList;

import com.vaadin.shared.ui.JavaScriptComponentState;

public class ChartState extends JavaScriptComponentState {
	private static final long serialVersionUID = 1L;
	public String ctype;
    public String domId;
    public String[] names;
    public String[] cat;
    public Double[][] val;
	public String title;
	public String xAxisTitle;
	public String yAxisTitle;
	public String ldomId;
	public ArrayList<String> unitsFromServer;
}
