package eu.iwidget.analytics.utility.sap.app;

/**
 * characterization of different filter types
 * @author kristina_kirsten
 *
 */
public enum FilterType{
	DROPDOWN, CHECKBOX, DATEPICKER,
	INPUT_FIELD, CAMPAIGNS_DATE_PICKER, UPLOAD,
	DEMAND_CHECKBOX, SLIDER, UPLOAD_PATTERN, COMPLAINTS_DATE_PICKER
}
