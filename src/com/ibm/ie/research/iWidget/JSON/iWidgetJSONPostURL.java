/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* OBJECT CODE ONLY SOURCE MATERIALS                                      */
/*                                                                        */
/* (C) COPYRIGHT International Business Machines Corp. 2013               */
/* All Rights Reserved                                                    */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/*                                                                        */
/*                                                                        */
/* @file iWidgetJSONPostURL.java                      					  */
/* @author Michael Barry                    (michael_barry@ie.ibm.com)    */
/*                                                                        */
/**************************************************************************/
package com.ibm.ie.research.iWidget.JSON;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class iWidgetJSONPostURL {	
	
	// make a REST call
	public String postURL(String URL, JSONObject args) {
		// setup the Apache Wink Client
		ClientConfig clientConfig = new ClientConfig().proxyHost("proxy").proxyPort(8080);
		RestClient client = new RestClient(clientConfig);

		// point the client to the desired REST URL
		Resource resource = client.resource(URL);
		System.out.println(URL);
//		System.out.println(args.toString());
		
		// make sure the result comes back as a JSON string
		resource.accept("application/json");

		// invoke the REST URL
		ClientResponse response = resource.post(args.toString());

		// do some checks on what we get back
		// DEBUG print the response code
		System.out.println("The Response Code is: " + response.getStatusCode());

		// need better error checking here and should throw an exception
		if (response.getStatusCode() != 200) {
			System.out.println("The Received Error is: " + response.getEntity(String.class));
			return null;
		}

		return response.getEntity(String.class);
	}

	// this function adds a new Analytic Component to the Central System
	public String postComponent(String postComponentURL, String componentName) {
		
		// Setup the component name as a JSON object
		// this is used as a parameter in the POST
		JSONObject componentNameJSON = new JSONObject();
		try {
			componentNameJSON.put("name", componentName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// invoke the REST URL
		String JSONString = postURL(postComponentURL, componentNameJSON);
		
		// DEBUG 
		if (JSONString != null) {
			try {
				JSONObject result = new JSONObject(JSONString);
				System.out.println(result.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
					
		return JSONString;
	}

	// this function adds a new Analytic Result to the Central System
	public String postResult(String postResultURL, String resultName, String parentID,
			JSONObject resultInputJSON, JSONObject resultOutputJSON, String executionDate) {
		
		// add all the inputs to a JSONObject
		// this JSONObject forms the parameter of the POST request
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put("name", resultName);
			resultJSON.put("parentID", parentID);
			resultJSON.put("executionDate",executionDate);
			resultJSON.put("input",resultInputJSON);
			resultJSON.put("output", resultOutputJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		// invoke the REST URL
		String JSONString = postURL(postResultURL, resultJSON);
		
		// DEBUG 
		if (JSONString != null) {
			try {
				JSONArray result = new JSONArray(JSONString);
				System.out.println(result.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
					
		return JSONString;
	}		
}
