/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* OBJECT CODE ONLY SOURCE MATERIALS                                      */
/*                                                                        */
/* (C) COPYRIGHT International Business Machines Corp. 2013               */
/* All Rights Reserved                                                    */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/*                                                                        */
/*                                                                        */
/* @file iWIdgetJSONGetURL.java                      							  */
/* @author Michael Barry                    (michael_barry@ie.ibm.com)    */
/*                                                                        */
/**************************************************************************/
package com.ibm.ie.research.iWidget.JSON;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class iWidgetJSONGetURL {

	// make a REST call
	public String getURL(String URL) {

		// setup the Apache Wink Client
        RestClient client = new RestClient(new ClientConfig().proxyHost("proxy").proxyPort(8080));
        // point the client to the desired REST URL
        Resource resource = client.resource(URL);
        System.out.println(URL);
        // make sure the result comes back as a JSON string
        resource.accept("application/json");

        // invoke the REST URL
        ClientResponse response = resource.get();

        // do some checks on what we get back
        // DEBUG print the response code
        System.out.println("The Response Code is: " + response.getStatusCode());

        // need better error checking here and should throw an exception
        if (response.getStatusCode() != 200) {
                        System.out.println("The Received Error is: "
                                                       + response.getEntity(String.class));
                        return null;
        }

        return response.getEntity(String.class);


	}
}
