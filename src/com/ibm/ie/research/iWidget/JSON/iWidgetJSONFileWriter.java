/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* OBJECT CODE ONLY SOURCE MATERIALS                                      */
/*                                                                        */
/* (C) COPYRIGHT International Business Machines Corp. 2013               */
/* All Rights Reserved                                                    */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/*                                                                        */
/*                                                                        */
/* @file iWIdgetJSONFileWriter.java            							  */
/* @author Michael Barry                    (michael_barry@ie.ibm.com)    */
/*                                                                        */
/**************************************************************************/
package com.ibm.ie.research.iWidget.JSON;

import java.io.BufferedWriter;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

public class iWidgetJSONFileWriter {


	public String writeDeviceList(BufferedWriter fileWriter, String JSONString) {


		JSONArray deviceArray = null;
		try {
			deviceArray = new JSONArray(JSONString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		// for each JSON object in the array print out the details
		// we know each JSON object is for a iWidget Device
		int i = 0;
		while (i < deviceArray.length()) {
			String deviceID = null;
			String parentID = null;
			try {
				JSONObject device = deviceArray.getJSONObject(i);
				deviceID = device.getString("id");
				if (!(device.isNull("parent")))
					parentID = device.getString("parent");
				fileWriter.write(deviceID + "," + parentID + "\n");
				
				//DEBUG 
				System.out.println("found device " + deviceID + " parent is " + parentID);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			i++;
		}
		// DEBUG
		System.out.println("Done");
		return "Done";
	}
}

