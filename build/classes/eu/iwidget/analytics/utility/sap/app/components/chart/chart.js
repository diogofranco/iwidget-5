var Chart = function(renderTo, ldomId, ctype, title,
		xAxisTitle, yAxisTitle, names, cat,
		val, element, unitsFromServer) {
	//The dojoConfig object is the primary mechanism for configuring Dojo in a web page or application. 
	//async: Defines if Dojo core should be loaded asynchronously.
	dojoConfig= {
			async: true
	};

	//Customize Dojo theme Dollar 
	define([
	        "dojox/charting/SimpleTheme",
	        "dojox/charting/themes/common"
	        ], function(SimpleTheme, themes){
		themes.Dollar = new SimpleTheme({
			colors: ["#663399","#990000","#006600","#6699FF","#636563"]});	
		return themes.Dollar;
	});

	require([
	         // Require the basic chart resource
	         "dojox/charting/Chart",

	         //Tooltip
	         "dojox/charting/action2d/Tooltip",

	         // Require the theme of our choosing
	         "dojox/charting/themes/Dollar",

	         //Legend
	         "dojox/charting/widget/Legend",

	         // 	Require the ClusteredColumns/Lines type of Plot 
	         "dojox/charting/plot2d/ClusteredColumns",
	         "dojox/charting/plot2d/StackedColumns",
	         "dojox/charting/plot2d/Scatter",
	         "dojox/charting/plot2d/Candlesticks",
	         "dojox/charting/plot2d/Columns",
	         "dojox/charting/plot2d/Lines",

	         //Don't delete "Default"!!
	         "dojox/charting/axis2d/Default",

	         // Wait until the DOM is ready
	         "dojo/domReady!", 
	         ], function(Chart, Tooltip, theme, Legend){

		//add some timeout to make sure that DOM is created 
		setTimeout(function(){

			if (ctype != "Candlesticks"){

				// Create the chart within it's "holding" node (renderTo)
				var chart = new Chart(renderTo, {title: title, titlePos: "top", titleGap: 25,
					titleFont: "normal normal normal 15pt Arial", titleFontColor: "black"});

				// Set the theme
				chart.setTheme(theme);
				// Add plot 
				chart.addPlot("default", {type: ctype, gap: 5, minBarSize: 3, maxBarSize: 50, tension:3, markers: true});
				//	chart.addPlot("other", {type: "Lines", markers: true});

				var multiplier = 1;
				if(unitsFromServer && (unitsFromServer[0] === "cms" ||
						unitsFromServer[0] === "cm.s")){
					multiplier = 3600;
				}

				if(title.slice(0,10) === "Downstream") multiplier = 1;
				console.log("multiplying values for " + multiplier);
				// Add the names and tooltip to the chart series
				for (var i=0; i < names.length; i++){
					var tmp = new Array();
					for (var j=0; j < val[i].length; j++){
						tmp[j] = {y: val[i][j] * multiplier , 
								tooltip: (yAxisTitle === "Performance Index (-)" ? "Time: " : "Date: ") + cat[j] + ", Value: " 
								+ (val[i][j] * multiplier).toFixed(2) + ", " + names[i]};
					}
					chart.addSeries(names[i], tmp);
				}

//				if(ctype=="ClusteredColumns"){
//				chart.addSeries("Series B", [{x:1, y:68}],{plot: "other", stroke:{color: "black"}});
//				}

				//Add categories to labels for x-axis
				var lbl = new Array();
				var tmp;
				for (var i=0; i<cat.length; i++){
					tmp = cat[i];
					lbl[i] = {value: i+1, text: tmp};
				}

				//Add x axis Line chart
				if(ctype == "Lines"){
					chart.addAxis("x",{title: xAxisTitle, titleOrientation: "away", 
						dropLabels: true, labels: lbl, rotation: -30,
						majorTickStep: 1, minorTicks: false, minorLabels: false});
				}
				//Add x axis ClusteredColumns chart
				else{
					chart.addAxis("x",{title: xAxisTitle, titleOrientation: "away", 
						minorLabels: false, minorTicks: false, labels: lbl});
				}

				//Add y axis 
				chart.addAxis("y", {title: yAxisTitle, vertical: true, includeZero: true,fixUpper : "major"});

				var tip = new Tooltip(chart, "default");
				chart.render();

				//Destroy previous legend
				var legend = dijit.byId(ldomId);
				if (legend) {
					legend.destroyRecursive(true);
				}

				//Add legend at the ldomId position referring to object chart (after chart rendering) 
				new Legend({chart: chart, outline: true}, ldomId);

			}

			else{
				var chartC = new Chart(renderTo, {title: title, titlePos: "top", 
					titleGap: 25, titleFont: "normal normal normal 15pt Arial", titleFontColor: "black"});
				chartC.setTheme(theme);
				var labl = new Array();
				var temp;
				var cInd;
				for (var j=0; j<names.length; j++){
					temp = names[j];
//					temp = "campaign ";
//					cInd = j + 1;
//					labl[j] = {value: j+1, text: temp + cInd};
					labl[j] = {value: j+1, text: temp };
				}
				var isCampaignsTab = title.slice(0,8) !== "Pressure";
				
				chartC.addAxis("xx", {title: (isCampaignsTab ? "Campaigns" : ""), titleOrientation: "away", 
					fixLower: "major", fixUpper: "major", 
					minorLabels: false, minorTicks: false, labels: labl});
				chartC.addAxis("yy", 
						{includeZero: true, title: yAxisTitle, vertical: true, fixLower: "major", 
					fixUpper: "major", natural: true});

				
				if(isCampaignsTab){
					chartC.addAxis("ybars", {includeZero: true, title: "Pressure (kPa)", vertical: true, fixLower: "major", 
						fixUpper: "major", natural: true, leftBottom: false});
					chartC.addPlot("other", {type: "Scatter", hAxis: "xx", vAxis: "ybars", markers: true});
				}


				chartC.addPlot("candle", {type: "Candlesticks", gap: 30 , hAxis: "xx", vAxis: "yy"});

				var tmp = new Array();
				var dot = new Array();
				var desc;

				for (var i=0; i < names.length; i++){
					tmp[i] = {open: val[i][0] , close: val[i][1] , high: val[i][2] , low: val[i][3] , mid: val[i][4], 
							tooltip: "open: " + val[i][0].toFixed(2) + " <br>close: " + val[i][1].toFixed(2) + 
							"<br>high: " + val[i][2].toFixed(2) + " <br>low: " + val[i][3].toFixed(2) +
							"<br>mid: " + val[i][4].toFixed(2)};

					var toolT = "";
					(val[i][5]) ? toolT = val[i][5].toFixed(2) + " kPa": toolT = ""; 

					dot[i] = {x:(i+0.5), y: val[i][5], tooltip: toolT };
//					for (var j=0; j < val[i].length; j++){
//					var j=names
//					desc=cat[i];
//					}
					desc = cat[5];
				}
				chartC.addSeries("Percentile P5, P25, P50, P75, P95", tmp, {plot: "candle", stroke: 
				{color:"#003399"}, fill: "#3399cc"});
				if(isCampaignsTab)
					chartC.addSeries(desc, dot, {plot: "other", stroke:{color: "black"}});
				//chartC.addSeries("Series B", [{x:(i-0.5),  y:val[i][5]}],{plot: "other", stroke:{color: "black"}});
				//for (var i=0; i < names.length; i++){
				//for (var j=0; j < val[i].length; j++){
				//chartC.addSeries("Series C", [{ open: val[i][0], close: val[i][1], high: val[i][2], low: val[i][3], mid: val[i][4]}], {plot: "candle", stroke: { color: "green" }, fill: "lightgreen" });
				//chartC.addSeries("Series C", [{  open: 16, close: 22, high: 26, low: 6, mid: 18}], {plot: "candle", stroke: { color: "green" }, fill: "lightgreen" });
				//	chartC.addSeries("Series B", [{ x: 0.2, y:val[i][5]}],{plot: "other", stroke:{color: "black"}});
				//	}
				//}

				new Tooltip(chartC, "candle");
				if(isCampaignsTab)
					new Tooltip(chartC, "other");
				chartC.render();
				var legend = dijit.byId(ldomId);
				if (legend) {
					legend.destroyRecursive(true);
					new Legend({chart: chartC, outline: true}, ldomId);
				}

				//Add legend at the ldomId position referring to object chart (after chart rendering) 
				new Legend({chart: chartC, outline: true}, ldomId);
			}
		},200);
	});

	//Setters for getting values from server
	this.setDomId = function (DomId) {renderTo = DomId;};
	this.setLdomId = function (LdomId) {ldomId = LdomId;};
	this.setNames = function (Names) {names = Names;};
	this.setCat = function (Cat) {cat = Cat;};
	this.setVal = function (Val) {val = Val;};
	this.setCtype = function (Ctype) {ctype = Ctype;};
	this.setTitle = function (Title) {title = Title;};
	this.setxAxisTitle = function (XAxisTitle) {xAxisTitle = XAxisTitle;};
	this.setyAxisTitle = function (YAxisTitle) {yAxisTitle = YAxisTitle;};
	this.setUnitsFromServer = function (UnitsFromServer) {unitsFromServer = UnitsFromServer;};
};