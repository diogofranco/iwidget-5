eu_iwidget_analytics_utility_sap_app_components_chart_Chart = function () {

	var element = this.getElement();
	var domId = this.getState().domId;
	var ldomId = this.getState().ldomId;
	var ctype = this.getState().ctype;
	var title = this.getState().title;
	var xAxisTitle = this.getState().xAxisTitle;
	var yAxisTitle = this.getState().yAxisTitle;
	var unitsFromServer = this.getState().unitsFromServer;
	var names = new Array(); names = this.getState().names;
	var cat = new Array(); cat = this.getState().cat;

	//diogo - hackish css injection for chart layout
	link = document.createElement( "link" );
	link.href = "http://archive.dojotoolkit.org/nightly/dojotoolkit/dijit/themes/claro/claro.css";
	link.type = "text/css";
	link.rel = "stylesheet";
	link.media = "screen,print";
	document.getElementsByTagName( "head" )[0].appendChild( link );
	
	//Canvas element <div> for Chart
	var canvasElement = document.createElement('div');
	canvasElement.setAttribute('id', domId);
	canvasElement.setAttribute('style', "width: 800px; height: 300px;");
	element.appendChild(canvasElement);
	
	
	
	//Canvas element <div> for Legend
	var canvasElement2 = document.createElement('div');
	canvasElement2.setAttribute('id', ldomId);
	canvasElement2.setAttribute('style', "width: 200px; height: 50px;");
	element.appendChild(canvasElement2);
	
	// Create the chart
    var chart = new Chart(domId, ldomId, ctype, title, xAxisTitle, yAxisTitle, names, cat, this.getState().val, element, unitsFromServer);
    
    // Handle changes from the server-side
    this.onStateChange = function () {
    	chart.setDomId(this.getState().domId);
    	chart.setLdomId(this.getState().ldomId);
    	chart.setNames(this.getState().names);
    	chart.setCat(this.getState().cat);
    	chart.setVal(this.getState().val);
    	chart.setCtype(this.getState().ctype);
    	chart.setTitle(this.getState().title);
    	chart.setxAxisTitle(this.getState().xAxisTitle);
    	chart.setyAxisTitle(this.getState().yAxisTitle);
    	chart.setUnitsFromServer(this.getState().unitsFromServer);
    };
};

