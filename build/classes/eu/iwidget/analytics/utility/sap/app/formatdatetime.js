$(document).ready(function(){
            
        var format = '15min';
        
        var dateTime = "2009-01-25 01:30:24";
        
        console.log("original: " + dateTime);
        
        console.log(format + ": \t" +getFromattedDateTime(dateTime, format));
       
        format = 'hour';
        console.log(format + ": \t" +getFromattedDateTime(dateTime, format));
       
        format = 'day';
        console.log(format + ": \t" +getFromattedDateTime(dateTime, format));
       
        format = 'week';
        console.log(format + ": \t" +getFromattedDateTime(dateTime, format));
       
        format = 'month';
        console.log(format + ": \t" +getFromattedDateTime(dateTime, format));
        
        
//        $.event.trigger('eventName'); 
//        
        setTimeout( function(){ 
        		console.log($('g.highcharts-axis').length + " axis");
        		registerAxisModifyEvent();
        	}, 1000);
//        
//        
//        $('g.highcharts-axis').change(function(){
//	        alert("change");
//	        $('g.highcharts-axis text').each(function(){ 
//	        	var text = "";
//	        	$(this).children('tspan').each(function(){
//	        		text += $(this).val();
//	        	});
//	        	console.log(text)
//	        });
//        });



	});
    

	var sResolution;
	
	
	$('div#resolutionFilter input').change(function(){


//	$('div#VAADIN_COMBOBOX_OPTIONLIST div.popupContent div.v-filterselect-suggestmenu td.gwt-MenuItem.gwt-MenuItem-selected').change(function(){
			var v = $(this).text();
			alert("Change " + v);
		});

	/**
	 * 
	 */
	function modifyDates(resolution){
		 
//	    $('g.highcharts-axis').change(function(){
//	        alert("change");
//			var resolution = $('div#VAADIN_COMBOBOX_OPTIONLIST div.popupContent div.v-filterselect-suggestmenu td.gwt-MenuItem.gwt-MenuItem-selected').text();
	        switch(resolution){
	        	case 'Hourly':	resolution = 'hour';
								break;
	        	case 'Weekly':	resolution = 'week';
								break;
	        	case 'Monthly':	resolution = 'month';
								break;
	        	case 'Daily':	resolution = 'day';
								break;
	        	case '15 Min':	resolution = '15min';
								break;
	        }
			$('g.highcharts-axis text').each(function(){ 
	        	var text = "";
	        	$(this).children('tspan').each(function(){
	        		text += $(this).text();
	        	});
	        	//$(this).children('tspan').remove();
	        	console.log(text)
	        	var fText;
	        	if(text.match(/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/g)){
	        		console.log("YES");
	        		fText = getFromattedDateTime(text, resolution);
	        		console.log(fText);
	        	}else console.log("no")
//	        	$(this).children('tspan').text()
	        	
	        });
//	    });
	}

	/**
	 * 
	 * @param str
	 * @returns
	 */
    function addZeros(str){
        var num = parseInt(str);
        return (num < 10)? "0" + num : "" + num;
    }
    
    
    /**
     * 
     */
    function getFromattedDateTime(dateTime, format){
        
        var dateParts = dateTime.split(" ")[0].split("-");
        var timeParts = dateTime.split(" ")[1].split(":");
        
        var date = new Date(dateParts[0], dateParts[1], dateParts[2], timeParts[0], timeParts[1], timeParts[2]);
        
        var formatted = "";
        
        switch(format){
            case '15min':   formatted = addZeros(date.getHours()) + ":" + addZeros(date.getMinutes());
                            break;
            case 'hour' :   formatted = addZeros(date.getHours()) + ":00";
                            break;
            case 'day'  :   formatted = addZeros(date.getDate()) + "." + addZeros(date.getMonth()+1) + ".";
                            break;
            case 'week' :   var weekDate = getWeekNumber(date);
                            formatted = addZeros(weekDate[1]) + "-" + (weekDate[0]+"").substr(2);
                            break;
            case 'month':   formatted = addZeros(date.getMonth()+1) + "." + ((date.getFullYear()+"").substr(2));
                            break;
            default     :   formatted = undefined;
        }
        return formatted;
    }
    
    /**
     * retruns yeaer and weeknumber
     * see http://stackoverflow.com/questions/6117814/get-week-of-year-in-javascript-like-in-php
     */
    function getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(d);
        d.setHours(0,0,0);
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setDate(d.getDate() + 4 - (d.getDay()||7));
        // Get first day of year
        var yearStart = new Date(d.getFullYear(),0,1);
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7)
        // Return array of year and week number
        return [d.getFullYear(), weekNo];
    }